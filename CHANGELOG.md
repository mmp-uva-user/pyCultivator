## Version 0.1.6
* [Bug] Fix problem with turbidostat
* [Bug] Improve configuration loading
* [Bug] Prevent crash when turbidostat table is missing
* [Bug] Fix negation of doReporting and doSimulate
* [Feature] Improve turbidostat data model
* [Feature] Improve random generation of OD Values when simulating

## Version 0.1.5.4
* [Bug] Fix problems with calibration models
* [Bug] Fix problems with commanding Reglo ICC pumps

## Version 0.1.5.3
* [Bug] Fix calibrator GUI

## Version 0.1.5.2
* [Bug] Fix multi-process logging

## Version 0.1.5.1
* [Documentation] Update documentation

## Version 0.1.5
* [Bug] Fix turbidostat protocol
* [Feature] Implement aggregation of OD Measurements 
* [Feature] Implement writing to SQLite by Turbidostat Protocol
* [Maintenance] Deprecate all exporter classes except SQLite
* [Maintenance] Rename arguments to use dashes instead of underscores
* [Maintenance] Adopt better git branching model

## Version 0.1.4
* [Bug] Fix issue where receiving multiple messages lead to incorrect data handling.
* [Bug] Fix issue with switching lights state
* [Feature] Improve logging, use '-v' to set log level
* [Bug] Reduce number of refreshLightSettings calls when loading config
* [Feature] Upgrade to pySerial 3.x

## Version 0.1.3
* [Bug] Tolerate spaces in domain notation(#40 on Redmine) 
* [Bug] Make light elements optional in channels
* [Maintenance] Some code cleaning

## Version 0.1.2

* [Feature] INI-files can be used to set global configuration
* [Feature] Added SQLite support

## Before version 0.1.2

* First release
