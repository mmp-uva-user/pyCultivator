# pyCultivator and the Multi-Cultivator

This document describes how to configure a minimal experimental set-up using a computer, pyCultivator and a Multi-Cultivator.

## General workflow

The package contains scripts that can be executed from the command-line.
These scripts contain the intelligence of the package and work according to a "execute-and-die" principle.

All configuration is done from scratch and the script will terminate after the job has been performed.
This allows for much simpler programs as there are no dependencies on previous states or activities, 
making it easier to recover from failures. 

Cron-jobs (OS-wide timers) are used to automate the timed execution of the scripts.

## Requirements

### Hardware requirements

* working computer
* internet connection (for remote interaction)
* server (for remote interaction)
* Serial cable
* Serial-to-USB converter
* USB Cable
* (Possibly) USB hub

### Software requirements

* pyCultivator package
* Unix environment (CentOS is known, Debian/Ubuntu is fine)
* Python 2.7 (not 3.x)
* Python Packages
	* lxml (config)
	* ~~pyorient (orientdb)~~
	* numpy
	* scipy
	* pySerial
	* PyQt4 (for GUI)
* git
* cron job
* automatic assignment of USB ports to tty sockets

## Moving parts

### Computers

pyCultivator is developed on unix-derived Operating Systems (OS). It is tested on
Mac OS X and CentOS. Other similar OS (Debian, Ubuntu) should work fine.

Deploying pyCultivator on a Windows Machine should be possible, but has not been tested yet.
The following software parts are most likely to work differently or fail on Windows:

* Cron-jobs (scheduled execution of script)
* Bash scripts
* udev configuration for Serial connections
* Python environment and package installation
* File paths

### pyCultivator

Two versions of pyCultivator currently exist. Because the difference between these packages is huge,
they are contained in different repositories.

* __pycultivator__: Currently in development, next major release
* __pycultivator_legacy__: Prototype version, stable

#### pyCultivator

The __pycultivator__ package is a complete redesign aimed to be more versatile and less focused on Multi-Cultivators.
This version can be installed as a true python package:

```bash
# core package 
pip install git+https://git.jongbloets.net/pycultivator.git
# bindings for Multi-Cultivator
pip install git+https://git.jongbloets.net/pycultivator-psimc.git
```

Updates can be installed with pip using the `--upgrade` parameter.

The rest of the document is written for __pycultivator_legacy__.

#### pyCultivator-legacy

The prototype package developed specifically for the Multi-Cultivator MC-1000-OD of Photon Instruments Systems (PSI).
This is not a true package, so the source files are downloaded directly from a repository.

```bash
cd /opt
# clone into /opt/pycultivator
git clone https://gitlab/mmp-uva/pyCultivator.git pycultivator
```

To update the package just `pull` the latest version. Bare in mind that this will overwrite any uncommitted changes.

```bash
cd /opt/pycultivator
git pull
```

### Filesystem structure
 
In general a good structure is as follows:

* `/opt/pycultivator`: Contains source files (only __pycultivator_legacy__)
* `/var/multicultivator`: Contains data and configuration files.

### Configuration files

Two types of configuration files exists: `.ini` and `.xml`. Examples can be found in the `examples` folder.

The `.ini` contains the global configuration while `.xml` contains the experiment specific configuration. `.ini` files describe the location of the data, log and configuration files and may also set general settings.

A script (command-line executable, found in the `uvaTools` folder) will look for `.ini` files in the following locations (in this order):

1. The working directory from which the script was called
2. The directory where the script is located (usually `/opt/pycultivator/uvaTools`
3. The parent directory of the script location (usually `/opt/pycultivator`)

It is advisable to put the `.ini` file in `/opt/pycultivator` 

### Automatic measurement

Automatic measurements can be implemented by using a cron-job and a simple bash script:

```bash
#!/bin/bash
#
#	file: /usr/local/bin/run_measure.sh
#	run-as: mcuser (or any other user member of dialout group)
#
PYCULTIVATOR_DIR="/opt/pycultivator"

cd $PYCULTIVATOR_DIR
# download latest version
git pull
# run python script
python tools/batch_cultivation.py
```

This script will first go to the the directory with the source and download the latest version of the source files.
After this the script runs a python script (`batch_cultivation.py`) to run the cultivation protocol for all active configurations.

An `.ini` file in `/opt/pycultivator` can be used to provide the location of the `.xml` configuration files.

To configure automatic execution of this script we create a cron-job, that runs every 5 minutes.

```cron
# m		h	dom		mon		dow		command
  */5	*	*		*		*		/usr/local/bin/run_measure.sh
```

### Automatic assignment of USB ports to sockets

In order to be able to communicate with the correct Multi-Cultivator, one has to chose the correct socket on the computer and enter this in the configuration file. USB sockets may change every time a USB cable is reconnected (or the computer restarts). This requires a program (on CentOS: udev) that is able to recognize the connected Multi-Cultivators and make it available under a static name. The Multi-Cultivators do not advertise themselves under an unique identifier. The Serial-to-USB converter, however, does and can be used to identify the Multi-Cultivator (provided the converter is not changed).  

On CentOS, udev rules are used to automatically create links to right socket and name them after the Multi-Cultivator ID (i.e. `/dev/ttyMC1`, `/dev/ttyMC2`, etc.).

Excerpt from `/etc/udev/rules.d/99-mc-usb-serial.rules`:

```bash
SUBSYSTEM=="tty", ATTRS{idVendor}=="0403", ATTRS{idProduct}=="6001", ATTRS{serial}=="FTAK08XU" SYMLINK+="ttyMC0" GROUP="dialout"
```

This defines a rule that associates the converted named `FTAK08XU` (with Vendor ID `0403` and Product ID `6001`) with `MC_0` and creates the symlink `/dev/ttyMC0` pointing to the socket connected to this device.

More about udev rules:

* http://hackaday.com/2009/09/18/how-to-write-udev-rules/
* http://reactivated.net/writing_udev_rules.html


### Data storage

Different drivers have been developed (or are being developed) to support different DBMS (Database Management Systems). The following table summarizes their capabilities.

| Name     | Stage      | Storage location | Remote sync? | Data access                  |
| -------- | ---------- | ---------------- | ------------ | ---------------------------- |
| CSV      | Broken     | Local (files)    | GIT / rsync  | R, Python^1, Excel           |
| OrientDB | Production | Remote + local   | Possible     | R^2, Python^3                |
| mongoDB  | Broken     | Remote + local   | Possible     | R, Python^7                  |
| sqlite   | Production | Local            | GIT / rsync  | R, Python^1, SQLiteBrowser^4 |
| monetdb  | Planned    | Local + remote   | GIT / rsync  | R, Python^5                  |
| mysql    | Planned   | Remote           | No           | R, Python^6                  | 

^1 : Built-in. ^2 : Not working properly. ^3 : Requires extra package (pyorient). ^4 : Third-party software

^5 : Requires extra package (python-monetdb). ^6 : Requires extra package (mysql-python or pymysql, not final yet)

^7 : Requires extra package (Ming)

Remote storage requires a working internet connection during the whole experiment. Although internet connections  are reasonably reliable nowadays, this does introduce a dependency that could lead to failure. This is especially true for protocols that rely on access to the history of measurements. It is therefore safer to first store data locally and then synchronise over the internet.
