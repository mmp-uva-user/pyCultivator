# coding=utf-8
"""
Initialize stub for uvaCultivator package
"""

from uvaObject import uvaObject, abstractmethod
import uvaLog

__author__ = 'Joeri Jongbloets <j.a.jongbloets@student.vu.nl>'

# connect to logger
uvaLog.getLogger(__name__).debug("Connected {} module to logger".format(__name__))
