"""Module testing the functions of the objects in the uvaPolynomial module"""

__author__ = 'Joeri Jongbloets <j.a.jongbloets@uva.nl>'

import unittest
from uvaCultivator import uvaPolynomial


class DomainTest(unittest.TestCase):

    _subject_class = uvaPolynomial.Domain

    def __init__(self, methodName='runTest'):
        super(DomainTest, self).__init__(methodName=methodName)

    @classmethod
    def getSubjectClass(cls):
        """

        :return:
        :rtype: uvaCultivator.uvaPolynomial.Domain
        """
        return cls._subject_class

    def test_CreateDomain(self):
        d = self.getSubjectClass().createDomain()
        self.assertIs(self.getSubjectClass().createDomain(), d, msg="Expect object is only created once")
        self.assertIsNot(self.getSubjectClass().createDomain(start=1), d, msg="Cannot create different object")

    def test_createFromString(self):
        # test simple domain
        d = self.getSubjectClass().createFromString("(,)")
        self.assertIsInstance(d,uvaPolynomial.Domain)
        self.assertIsNone(d.getStartValue())
        self.assertIsNone(d.getEndValue())
        self.assertFalse(d.includesStart())
        self.assertFalse(d.includesEnd())
        # test inclusion
        d = self.getSubjectClass().createFromString("[,]")
        self.assertIsNone(d.getStartValue())
        self.assertIsNone(d.getEndValue())
        self.assertTrue(d.includesStart())
        self.assertTrue(d.includesEnd())
        # test integers
        d = self.getSubjectClass().createFromString("[1,]")
        self.assertEqual(d.getStartValue(), 1)
        self.assertIsNone(d.getEndValue())
        d = self.getSubjectClass().createFromString("[1,2]")
        self.assertEqual(d.getStartValue(), 1)
        self.assertEqual(d.getEndValue(), 2)
        # test floats
        d = self.getSubjectClass().createFromString("[1.5,2]")
        self.assertEqual(d.getStartValue(), 1.5)
        self.assertEqual(d.getEndValue(), 2)
        d = self.getSubjectClass().createFromString("[1.5,.2]")
        self.assertEqual(d.getStartValue(), 1.5)
        self.assertEqual(d.getEndValue(), 0.2)
        d = self.getSubjectClass().createFromString("[1.5,0.2]")
        self.assertEqual(d.getStartValue(), 1.5)
        self.assertEqual(d.getEndValue(), 0.2)
        # test spaces
        d = self.getSubjectClass().createFromString("[ 1,]")
        self.assertEqual(d.getStartValue(), 1)
        self.assertIsNone(d.getEndValue())
        d = self.getSubjectClass().createFromString("[1, 2]")
        self.assertEqual(d.getStartValue(), 1)
        self.assertEqual(d.getEndValue(), 2)
        # test floats
        d = self.getSubjectClass().createFromString("[ 1.5 , 2 ]")
        self.assertEqual(d.getStartValue(), 1.5)
        self.assertEqual(d.getEndValue(), 2)
        d = self.getSubjectClass().createFromString("[1.5, .2 ]")
        self.assertEqual(d.getStartValue(), 1.5)
        self.assertEqual(d.getEndValue(), 0.2)
        d = self.getSubjectClass().createFromString("[1.5, 0.2 ]")
        self.assertEqual(d.getStartValue(), 1.5)
        self.assertEqual(d.getEndValue(), 0.2)



class PolynomialTest(unittest.TestCase):
    _subject_class = uvaPolynomial.Polynomial

    def __init__(self, methodName='runTest'):
        super(PolynomialTest, self).__init__(methodName=methodName)

    @classmethod
    def getSubjectClass(cls):
        """

        :return:
        :rtype: uvaCultivator.uvaPolynomial.Polynomial
        """
        return cls._subject_class