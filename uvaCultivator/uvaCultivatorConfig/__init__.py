# coding=utf-8
"""
Initialize stub for uvaCultivatorData package
"""

from .. import uvaLog

__author__ = 'Joeri Jongbloets <j.a.jongbloets@uva.nl>'

# connect to logger
uvaLog.getLogger(__name__).debug("Connected {} to logger".format(__name__))
