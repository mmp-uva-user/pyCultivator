# coding=utf-8
"""
Initialize stub for uvaCultivatorExport package
"""

from .. import uvaLog

__author__ = 'Joeri Jongbloets <j.a.jongbloets@uva.nl>'

# connect to logger
uvaLog.getLogger(__name__).debug("Connected {} to logger".format(__name__))

#CULTIVATOR_DATA_CSV = "CSV".lower()
#CULTIVATOR_DATA_ORIENT = "ORIENT".lower()
#CULTIVATOR_DATA_MONGO = "MONGO".lower()
CULTIVATOR_DATA_SQLITE = "SQLITE".lower()


def getClassByName(name, settings=None, namespace=None):
    """Return the Exporter Class that matches the given name and store it as setting.

    :param name: Name of Exporter Class (CSV, orient etc.)
    :type name: str
    :param settings:
    :type settings: dict
    :param namespace: Namespace of the class in the settings (exporter or importer)
    :type namespace: str
    :return: Class reference
    :rtype:
    """
    if settings is None:
        settings = {}
    if namespace is None:
        namespace = ""
    else:
        namespace = "{}.".format(namespace)
    uCE = None
    # if name.lower() == CULTIVATOR_DATA_CSV:
    #     from .csvCultivatorExport import PortraitCSVExport as uCE
    # if name.lower() == CULTIVATOR_DATA_MONGO:
    #     from .mongoCultivatorExport import SimpleMongoExport as uCE
    # if name.lower() == CULTIVATOR_DATA_ORIENT:
    #     from .orientCultivatorExport import orientCultivatorExport as uCE
    if name.lower() == CULTIVATOR_DATA_SQLITE:
        from .sqliteCultivatorExport import SQLiteCultivatorExport as uCE
    if uCE is not None:
        settings["{}name".format(namespace)] = name
        settings["{}class".format(namespace)] = uCE
    return settings
