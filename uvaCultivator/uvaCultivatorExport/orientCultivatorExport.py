# coding=utf-8
#!/usr/bin/env python
"""
OrientDB Handler
"""

__author__ = 'Joeri Jongbloets <j.a.jongbloets@student.vu.nl>'

from uvaCultivatorExport import AbstractCultivatorExport as aCE
from uvaCultivator.uvaODMeasurement import ODMeasurement as odM
#from uvaOrientModel import *
from datetime import datetime as dt, timedelta as td
from collections import deque
import pyorient, re
from pyorient.exceptions import *

class orientCultivatorExport(aCE):

	# URL_TEST is a regex pattern that can process: [<protocol>://][<user>:<password>@]<server>[:<port>][/<database>]
	URL_TEST = re.compile('^(?:([^:/]+)://)?(?:([^:]+):([^@]+)@)?([^@:/?#]+)(?::([0-9]+))?/?(?:([^/]+))?')
	STATE_UNCONNECTED = 0
	STATE_HOST_CONNECTED = 1
	STATE_DB_CONNECTED = 2

	ORIENT_STORAGE = 'orientdb'     # for legacy
	ORIENT_MEMORY_STORAGE = 'memory'
	ORIENT_PLOCAL_STORAGE = 'plocal'
	ORIENT_STORAGE_MAP = {
		ORIENT_MEMORY_STORAGE: pyorient.STORAGE_TYPE_MEMORY,
		ORIENT_PLOCAL_STORAGE: pyorient.STORAGE_TYPE_PLOCAL,
		ORIENT_STORAGE: pyorient.STORAGE_TYPE_PLOCAL
	}
	ORIENT_DEFAULT_STORAGE = ORIENT_PLOCAL_STORAGE

	URL_FMT = "{protocol}://{auth}{host}/{database}"

	_default_settings = aCE.mergeDictionary(
		aCE._default_settings,
		{
			"project_id": "",
			"t_zero": None,
		})

	_default_url_settings = {
		"protocol" : ORIENT_PLOCAL_STORAGE,
		"username" : "sils_user",
		"password" : "#MC$",
		"server" : "localhost",
		"port" : 2424,
		"database" : "mc_production"
	}


	def __init__(self, settings=None):
		aCE.__init__(self, settings, check_storage=False)
		if "url" in self._settings.keys():
			self.setDatabaseURL(self._settings["url"])
		self._state = self.STATE_UNCONNECTED
		self._client = None
		self._session = None
		self._schema = None
		self._data_cache = deque()
		""":type: deque[uvaCultivator.uvaODMeasurement.ODMeasurement] """
		self._experiment = None


	def __exit__(self, exc_type, exc_val, exc_tb):
		if self.isConnected():
			self.disconnect()

	def getConnectionState(self):
		return self._state

	def getClient(self):
		"""
		:return: pyOrient Client Object
		:rtype: pyorient.OrientDB
		"""

		return self._client

	def getSchema(self):
		"""
		:return: Schema dictionary
		:rtype: pyorient.Information
		"""
		return self._schema

	def hasSchema(self):
		return self.getSchema() is not None

	def getSession(self):
		return self._session

	def isConnected(self, min_state=STATE_HOST_CONNECTED):
		return self.getConnectionState() >= min_state

	def isInState(self, state=STATE_DB_CONNECTED):
		return self.getConnectionState() == state

	def hasStorage(self):
		"""
		:return: Return whether we can connect to the server and create a database (if not already present)
		:rtype: bool
		"""
		return self.isValidURL(self.getDatabaseURL())

	def createStorage(self, destination=None, replace=False):
		"""
		Tries to connect to the database using the destination url string
		:type destination: str
		:return: True on success, False on failure
		:rtype: bool
		"""
		try:
			# test for connection to the server
			if not self.isConnected():
				self.connect(desired_state=self.STATE_HOST_CONNECTED)
			# test if db exists
			if not self.getClient().db_exists(self.getDatabase()):
				# ok we do have a connection, but not a database, create it!
				# default use the plocal
				storage_type = "plocal"
				if self.getDatabaseProtocol() in self.ORIENT_STORAGE_TYPES:
					# if known storage type is given: use it
					storage_type = self.getDatabaseProtocol()
				self.getClient().db_create(self.getDatabase(),
				                           pyorient.DB_TYPE_GRAPH,
				                            storage_type
				                           )
			# connect to database
			self._schema = self.getClient().db_open(self.getDatabase(), self.getDatabaseUser(),
			                                        self.getDatabasePassword())
		# create schema
		# TODO : Implement create Schema
		except Exception as e:
			self.getLog().critical(e)
		return self.hasStorage()

	def hasClass(self, name):
		result = False
		self.connect(desired_state=self.STATE_DB_CONNECTED)
		if self.getSchema() is not None:
			for cluster in self.getSchema():
				#result = "name" in item.keys() and name.lower() == item["name"].lower()
				if cluster.name.lower() == name.lower():
					result = True
					break
		return result

	def getCluster(self, name=None):
		"""
		Returns the cluster ID of the class
		:param name:
		:type name: str
		:return: ID of cluster
		:rtype: None or int
		"""
		result = None
		self.connect(desired_state=self.STATE_DB_CONNECTED)
		if self.getSchema() is not None:
			for cluster in self.getSchema():
				#if "name" in cluster.keys() and name.lower() == cluster["name"].lower():
				if cluster.name.lower() == name.lower():
					result = cluster.id
					break
		return result

	def hasClasses(self):
		result = self.hasClass("experiment")
		result = result and self.hasClass("has_channel")
		result = result and self.hasClass("channel")
		result = result and self.hasClass("date")
		result = result and self.hasClass("year")
		result = result and self.hasClass("has_year")
		result = result and self.hasClass("month")
		result = result and self.hasClass("has_month")
		result = result and self.hasClass("day")
		result = result and self.hasClass("has_day")
		result = result and self.hasClass("measurement")
		result = result and self.hasClass("mcMeasurement")
		result = result and self.hasClass("has_measurement")
		return result

	def createClasses(self):
		result = None
		try:
			# test experiment
			if not self.hasClass("experiment"):
				self.getClient().batch("""
				create class Experiment extends V
				create property Experiment.project_id string
				create property Experiment.t_zero datetime
				create property Experiment.state boolean
				""")
			if not self.hasClass("channel"):
				self.getClient().batch("""
				create class Channel extends V
				create property Channel.channel_id integer
				""")
			if not self.hasClass("has_channel"):
				self.getClient().batch("""
				create class has_channel extends E
				create property has_channel.in LINK Channel
				create property has_channel.out LINK Experiment
										""")
			if not self.hasClass("date"):
				self.getClient().batch("""
				create class Date extends V
				create property Date.value integer
				create property Date.size integer
							""")
			if not self.hasClass("year"):
				self.getClient().batch("""
				create class Year extends Date
				""")
			if not self.hasClass("has_year"):
				self.getClient().batch("""
				create class has_year extends E
				create property has_year.in LINK Year
				create property has_year.out LINK Channel
				""")
			if not self.hasClass("month"):
				self.getClient().batch("""
				create class Month extends Date
				""")
			if not self.hasClass("has_month"):
				self.getClient().batch("""
				create class has_month extends E
				create property has_month.in LINK Month
				create property has_month.out LINK Year
				""")
			if not self.hasClass("day"):
				self.getClient().batch("""
				create class Day extends Date
				""")
			if not self.hasClass("has_day"):
				self.getClient().batch("""
				create class has_day extends E
				create property has_day.in LINK Day
				create property has_day.out LINK Month
				""")
			if not self.hasClass("Measurement"):
				self.getClient().batch("""
				create class Measurement extends V
				create property Measurement.time datetime
				""")
			if not self.hasClass("mcMeasurement"):
				self.getClient().batch("""
				create class mcMeasurement extends Measurement
				create property mcMeasurement.intensity float
				create property mcMeasurement.background float
				create property mcMeasurement.flash float
				create property mcMeasurement.od_led integer
				create property mcMeasurement.od_repeats integer
				create property mcMeasurement.od_value float
				create property mcMeasurement.od_raw float
				create property mcMeasurement.temperature float
				""")
			if not self.hasClass("has_measurement"):
				self.getClient().batch("""
				create class has_measurement extends E
				create property has_measurement.in LINK
				create property has_measurement.out LINK Day
				""")
		except Exception as e:
			self.getLog().critical("Exception raised in createClasses:\n {}".format(e))
		return result

	def connectToHost(self, server=None, port=None, host_user=None, host_password=None):
		"""
		Connect to the Orient Database Host
		:return: True on success, False on failure
		:rtype: bool
		"""
		if server is None:
			server = self.getDatabaseServer()
		if port is None:
			port = self.getDatabasePort()
		if host_user is None:
			host_user = self.getDatabaseUser()
		if host_password is None:
			host_password = self.getDatabasePassword()
		try:
			self._client = pyorient.OrientDB(server, port)
			if self._client is not None:
				if host_user not in self.EMPTY_VALUE and  host_password not in self.EMPTY_VALUE:
					self._session = self._client.connect(host_user, host_password)
				else:
					self._session = self._client.connect()
				if self._session is not None:
					self._state = self.STATE_HOST_CONNECTED
		except PyOrientConnectionException as pce:
			self._client = None
			self._session = None
			self.getLog().critical("[connectToHost] Unable to connect to {}:\n{}".format(server, pce))
		return None not in [self._client, self._session]

	def connectToDatabase(self, db=None, db_user=None, db_password=None):
		"""
		Connect to the Orient Database at the server
		:return: True on success, False on failure
		:rtype: bool
		"""
		if db is None:
			db = self.getDatabase()
		if db_user is None:
			db_user = self.getDatabaseUser()
		if db_password is None:
			db_password = self.getDatabasePassword()
		try:
			if self.getClient() is not None and db is not None:
				if self.hasDatabase():
					if db_user not in self.EMPTY_VALUE and db_password not in self.EMPTY_VALUE:
						self._schema = self.getClient().db_open(db, db_user, db_password)
					else:
						self._schema = self.getClient().db_open(db)
					if self._schema is not None:
						self._state = self.STATE_DB_CONNECTED
				else:
					raise Exception("[connectToDatabase] Database ({}) does not exist".format(db))
			else:
				if self.getClient() is None:
					raise Exception("[connectToDatabase] Please connect to host first")
				elif db is None:
					raise Exception("[connectToDatabase] Please set a proper Database Name")
		except PyOrientCommandException as pce:
			self._schema = []
			raise Exception("[connectToDatabase] Unable to connect to {}:\n{}".format(db, pce))
		return len(self._schema) > 0

	def connect(self, settings=None, desired_state=STATE_DB_CONNECTED, **kwargs):
		"""
		Connect to the database server
		:type settings: dict | None
		@kwargs: settings
		:return: Session token
		"""
		# merge settings with kwargs, kwargs replace settings
		settings = self.mergeDictionary(target=settings, source=kwargs)
		# create new URL use standard defaults
		new_url = self.createURL(settings=settings, default_url_settings=self._settings)
		# if url was given in the settings or kwargs; use that one
		if settings is not None and "url" in settings.keys():
			new_url = settings["url"]
		# proceed only when we have to change
		if new_url != self.getDatabaseURL() or desired_state != self.getConnectionState():
			# check if proposed URL is valid
			if self.isValidURL(new_url):
				try:
					# if we need to connect to a new URL while a connection is still open
					if new_url != self.getDatabaseURL() and self.isConnected():
						self.getLog().warning("There is still a connection to {}, so disconnect first..".format(
							self.getDatabaseURL()))
						self.disconnect()
						self.setDatabaseURL(new_url)
					# Disconnect if we need "downgrade" the connection state
					if self.getConnectionState() > desired_state:
						self.disconnect()
					# Connect if we are in a lower state and we need a higher state.
					if self.getConnectionState() < desired_state >= self.STATE_HOST_CONNECTED:
						self.connectToHost()
					# Connect if previous attempt was succesful
					if self.STATE_HOST_CONNECTED <= self.getConnectionState() < desired_state >= \
							self.STATE_DB_CONNECTED:
						self.connectToDatabase()
				except Exception as e:
					self.getLog().error(e)
			else:
				self.getLog().error("Unable to use the given URL to connect to the orientdb: {}".format(
					self.getDatabaseURL())
				)
		return self._session

	def disconnect(self):
		if self.isConnected(min_state=self.STATE_DB_CONNECTED):
			self.getClient().db_close()
			self._state = self.STATE_HOST_CONNECTED
		if self.isConnected(min_state=self.STATE_HOST_CONNECTED):
			self._client = None
			self._state = self.STATE_UNCONNECTED

	@classmethod
	def isValidURL(cls, url):
		"""
		Checks if a URL string is valid
		:param url: URl String to check
		:type url: str
		:return: Whether the URl string is valid
		:rtype: bool
		"""
		result = False
		if url is not None:
			result = len(cls.URL_TEST.findall(url)) > 0
		return result

	@classmethod
	def urlToSettings(cls, url, default_url_settings=None):
		"""
		Parse a url string to a settings dict
		"""
		## load defaults
		if default_url_settings is None:
			default_url_settings = cls._default_url_settings
		else:
			default_url_settings = cls.mergeDictionary(
				target=cls._default_url_settings,
				source=default_url_settings
			)
		## build url
		url_settings = {}
		if cls.isValidURL(url):
			## retrieve first matching element
			url_parts = cls.URL_TEST.findall(url)[0]
			url_settings = {
				"protocol" : url_parts[0],
				"username" : url_parts[1],
				"password": url_parts[2],
				"server" : url_parts[3],
				"port": url_parts[4],
				"database" : url_parts[5],
			}
		return cls.mergeDictionary(target=default_url_settings, source=url_settings)

	@classmethod
	def settingsToURL(cls, settings=None, default_url_settings=None, url_fmt=None, **kwargs):
		# load defaults for parameters
		if settings is None:
			settings = {}
		if default_url_settings is None:
			default_url_settings = cls.getDefaultURLSettings()
		if url_fmt is None:
			url_fmt = cls.URL_FMT
		# first replace defaults with given settings
		settings = cls.mergeDictionary(target=default_url_settings, source=settings)
		# then replace given settings with kwarg elements
		settings = cls.mergeDictionary(target=settings, source=kwargs)
		# build the auth part (if necessary)
		settings["auth"] = ""
		if settings["username"] not in cls.EMPTY_VALUE  and settings["password"] not in cls.EMPTY_VALUE:
			settings["auth"] = "{username}:{password}@".format(**settings)
		## build the host part
		settings["host"] = "{server}".format(**settings)
		if settings["port"] not in cls.EMPTY_VALUE:
			settings["host"] = "{server}:{port}".format(**settings)
		return url_fmt.format(**settings)

	def createURL(self, settings=None, default_url_settings=None, url_fmt=None, **kwargs):
		"""
		Create an URL String based on the elements in the settings (disregard the current URL).
		"""
		if settings is None:
			settings = self.getSettings()
		return self.settingsToURL(
			settings=settings,
			default_url_settings=default_url_settings,
			url_fmt=url_fmt,
			**kwargs
		)

	@classmethod
	def getDefaultURLSettings(cls):
		return cls._default_url_settings

	def getDatabaseURL(self, force=False):
		if force:
			# create new url based on the current settings and store it in the settings dict
			self._settings["url"] = self.createURL()
		return self.retrieveSetting(name="url", default=self.getDefaultSettings())

	def setDatabaseURL(self, url):
		"""
		If the given url is valid, update the settings dictionary to reflect the url
		:param url: URL String
		:type url: str
		:return: Return the new URL as stored in the settings dictionary.
		:rtype: str
		"""
		if self.isValidURL(url):
			# update the settings dict with the new url
			self._settings = self.urlToSettings(url, default_url_settings=self.getSettings())
		# return new url (and refresh it)
		return self.getDatabaseURL(force=True)

	def getDestination(self):
		return self.getDatabaseURL()

	def getDatabaseProtocol(self):
		return self.retrieveSetting(name="protocol", default=self.getDefaultSettings())

	def getStorageType(self):
		result = "plocal"
		if self.getDatabaseProtocol() in self.ORIENT_STORAGE_MAP.keys():
			result = self.ORIENT_STORAGE_MAP[self.getDatabaseProtocol()]
		return result

	def getDatabaseUser(self):
		return self.retrieveSetting(name="username", default=self.getDefaultSettings())

	def getDatabasePassword(self):
		return self.retrieveSetting(name="password", default=self.getDefaultSettings())

	def getDatabaseServer(self):
		return self.retrieveSetting(name="server", default=self.getDefaultSettings())

	def getDatabasePort(self):
		port = self.retrieveSetting(name="port", default=self.getDefaultSettings())
		if isinstance(port, str):
			try:
				port = int(port)
			except ValueError as ve:
				port = 2424
		return port

	def getDatabase(self):
		return self.retrieveSetting(name="database", default=self._default_settings)

	def hasDatabase(self, db_name=None):
		result = False
		if db_name is None:
			db_name = self.getDatabase()
		current_state = self.getConnectionState()
		self.connect(desired_state=self.STATE_HOST_CONNECTED)
		try:
			result = self.getClient().db_exists(db_name, self.getStorageType())
		except PyOrientCommandException as pce:
			self.getLog().error("Unable to check if {} exists at {}:\n{}".format(
				db_name,
				self.getDatabaseURL(),
				pce
			))
		if current_state > self.getConnectionState():
			self.connect(desired_state=current_state)
		return result

	def createDatabase(self, db_name=None):
		result = False
		if db_name is None:
			db_name = self.getDatabase()
		current_state = self.getConnectionState()
		self.connect(desired_state=self.STATE_HOST_CONNECTED)
		try:
			result = self.getClient().db_create(db_name, pyorient.DB_TYPE_GRAPH, self.getStorageType())
		except PyOrientCommandException as pce:
			self.getLog().error("Unable to create database {} at {}: \n{}".format(
				db_name,
				self.getDatabaseURL(),
				pce
			))
		if current_state > self.getConnectionState():
			self.connect(desired_state=current_state)
		return result

	def listDatabases(self):
		result = []
		# save current state
		current_state = self.getConnectionState()
		# change state if necessary
		self.connect(desired_state=self.STATE_HOST_CONNECTED)
		try:
			result = self.getClient().db_list().databases.keys()
		except PyOrientCommandException as pce:
			self.getLog().critical("Unable to get database list:\n{}".format(pce))
		# restore old state
		if current_state > self.getConnectionState():
			self.connect(desired_state=current_state)
		return result

	def query(self, query, parse_to=None, skip=0, limit=-1, fetch_plan="*:0"):
		results, records = [], []
		if self.isConnected(self.STATE_DB_CONNECTED):
			if skip > 0:
				query = "{} SKIP {}".format(query, skip)
			records = self.getClient().query(query, limit, fetch_plan)
		if parse_to is not None:
			for record in records:
				results.append(parse_to.fromRecord(self, record))
		else:
			results = records
		return results

	def getExperiment(self, force=False):
		if self._experiment is None and force:
			self._experiment = self.getExperimentRecord()
		return self._experiment

	def hasExperiment(self):
		return self.getExperiment() is not None

	# def hasExperimentRecord(self, project_id, t_zero):
	# 	return orientExperiment(self, project_id=project_id, t_zero=t_zero).exists()

	def createExperimentRecord(self, project_id, t_zero, state):
		result = None
		try:
			result = self.createVertex( "Experiment", project_id=project_id, t_zero=t_zero, state=state )
		except pyorient.PyOrientException as poe:
			self.getLog().critical("Unable to create Experiment record\n{}".format(poe))
		return result

	def getExperimentRecords(self, project_id, t_zero):
		result = []
		try:
			result = self.getClient().query(
				"select from Experiment where project_id='{project_id}' AND t_zero='{t_zero}'".format(
					project_id=project_id, t_zero=t_zero.strftime(self.DT_FORMAT)
				))
		except pyorient.PyOrientException as poe:
			self.getLog().critical("Unable to get Experiment record\n{}".format(poe))
		return result

	def getExperimentRecord(self, project_id=None, t_zero=None):
		if project_id is None:
			project_id = self.getProjectId()
		if t_zero is None:
			t_zero = self.getTimeZero()
		result = self.getExperimentRecords(project_id=project_id, t_zero=t_zero)
		return result[0] if len(result) > 0 else None

	def hasChannelRecord(self, experiment, channel_id):
		return len(self.getChannelRecords(experiment=experiment, channel_id=channel_id)) > 0

	def getChannelRecords(self, experiment, channel_id=None):
		result = []
		try:
			indexes = ""
			if channel_id is not None:
				indexes = " [channel_id={channel_id}]".format(channel_id=channel_id)
			query = "select from (select expand(out('has_channel')[@class=Channel]{indexes}) from {experiment})".format(
				experiment=experiment._rid,
				indexes=indexes
			)
			result = self.getClient().query(query)
		except pyorient.PyOrientException as poe:
			self.getLog().critical("Unable to get channel record:\n{}".format(poe))
		return result

	def getChannelCount(self, experiment, channel_id=None):
		result = 0
		try:
			indexes = ""
			if channel_id is not None:
				indexes = "[channel_id={channel_id}]".format(channel_id=channel_id)
			result = self.getClient().query(
				"select sum(out('has_channel'){indexes}.size()) AS channel_count from {experiment})".format(
					experiment=experiment._rid,
					indexes=indexes
				))
			result = result[0].channel_count if len(result) > 0 else 0
		except pyorient.PyOrientException as poe:
			self.getLog().critical("Unable to get channel record:\n{}".format(poe))
		return result


	def getChannelRecord(self, experiment, channel_id):
		result = self.getChannelRecords(experiment=experiment, channel_id=channel_id)
		return result[0] if len(result) > 0 else None

	def getChannelRecordsFromMeasurement(self, measurement):
		result = []
		try:
			if measurement._class.lower() == "measurement".lower():
				query = "select expand(in(\"has_measurement\").in(\"has_day\").in(\"has_month\").in(\"has_year\")"\
				        "[@class=Channel]) from {source}".format(
					source = measurement._rid
				)
				result = self.getClient().query(query)
		except pyorient.PyOrientException as poe:
			self.getLog().critical("Unable to get channel from {}:\n{}".format(measurement._rid, poe))
		return result

	def getChannelRecordFromMeasurement(self, measurement):
		result = self.getChannelRecordsFromMeasurement(measurement=measurement)
		return result[0] if len(result) > 0 else None

	def createChannelRecord(self, experiment, channel_id):
		result = None
		try:
			result = self.createVertex(rClass="Channel", channel_id=channel_id)
		except pyorient.PyOrientException as poe:
			self.getLog().critical("Unable to create channel record:\n{}".format(poe))
		if result is not None:
			#create property has_channel.in LINK Experiment
			# out = from ; in = to
			self.createEdge(from_rid=experiment._rid, to_rid=result._rid, name="has_channel")
		return result

	def hasMeasurement(self, channel, t_point, od_led=720, **attributes):
		"""
		Searches for a duplicate measurement. Add additional criteria by adding extra arguments where the name of the
		attributes should match the name of one of the attributes of the Measurement Class.
		:param channel: Record of the channel
		:type channel: pyorient.types.OrientRecord
		:type t_point: datetime.datetime
		:type od_led: int
		:return:
		:rtype:
		"""
		result = False
		criteria = self.makeCriteria(**attributes)
		criteria.append(("time", "=", t_point))
		try:
			# get day record to limit search
			year, month, day = self.getDateRecords(channel, t_point)
			# create search query
			where = " where {}".format(" and ".join(self.parseCriteria(*criteria)))
			if day is not None:
				query = "select count(*) as m_count from ( " \
						"select expand(out(\"has_measurement\")[od_led={od_led}])" \
						"from {source}){where}".format(
					od_led=od_led, source=day._rid, where=where
				)
				records = self.getClient().query(query, -1, "0:1")
				result = records[0].m_count > 0 if len(records) > 0 else False
		except pyorient.PyOrientException as poe:
			self.getLog().critical("Unable to find Measurement:\n{}".format(poe))
		return result

	def countMeasurementsForChannel(self, channel):
		result = 0
		try:
			query = "select sum(out(\"has_year\").out(\"has_month\").out(\"has_day\")" \
			        ".out(\"has_measurement\")[@class=mcMeasurement].size()) " \
			        "as m_count from {channel})".format(
				channel=channel._rid
			)
			result = self.getClient().query(query, -1, "0:1")
			result = result[0].m_count if len(result) > 0 else 0
		except pyorient.PyOrientException as poe:
			self.getLog().critical("Unable to count Measurements:\n{}".format(poe))
		return result

	def countMeasurements(self):
		result = 0
		try:
			exp_e = self.getExperiment()
			if exp_e is not None:
				# TODO: use traverse
				query = "select sum(out(\"has_channel\").out(\"has_year\").out(\"has_month\").out(\"has_day\")"
				".out(\"has_measurement\")[@class=mcMeasurement].size()) as m_count from {experiment}".format(
					experiment=exp_e._rid
				)
				result = self.getClient().query(query, -1, "0:1")
			result = result[0].m_count if len(result) > 0 else 0
		except pyorient.PyOrientException as poe:
			self.getLog().critical("Unable to get Measurement:\n{}".format(poe))
		return result

	def makeCriteria(self, operator="=", **criteria):
		"""
		Converts a set of criteria given as kwargs or dict to a list of criteria,
		:param criteria:
		:type criteria: dict
		:return: List of criteria of the form "<field><operator><value>".
		:rtype: list
		"""
		result = []
		for key in criteria.keys():
			result.append( "{}{}{}".format(key, operator, criteria[key]) )
		return result

	def parseCriteria(self, *criteria):
		"""
		Parse a list of criteria to statements of the form <field><operator><value>
		If the value in the list is a string, it will assume the str is already of the form <field><operator><value>.
		If the value in the list is a list or tuple, it matches the items in the list to
		[0][1][2] => <field><operator><value>
		If the value in the dict is a dict, it assumes it contains the keys "field", "operator" and "value".
		:param criteria: The dictionary to parse
		:type criteria: list[ str, str | tuple | list | dict ]
		:return: The list with match statements to be used for a where statement
		:rtype: list[str]
		"""
		results = []
		for item in criteria:
			field = ""
			operator = ""
			value = ""
			if isinstance(item, str):
				field = item
			elif isinstance(item, (tuple, list)):
				field = item[0]
				operator = item[1]
				value = item[2]
			elif isinstance(item, dict):
				if "field" in item.keys():
					field = item["field"]
				if "operator" in item.keys():
					operator = item["operator"]
				if "value" in item.keys():
					value = item["value"]
			if isinstance(value, dt):
				value = value.strftime(self.DT_FORMAT)
			if isinstance(value, str) and value != "":
				value = "\"{}\"".format(value)
			results.append("{field}{operator}{value}".format( field=field, operator=operator, value=value))
		return results

	def createPeriodCriteria(self, start_time=None, end_time=None, criteria=None):
		"""
		Creates a list of Criteria that will filter by time given the start and end time
		:param start_time:
		:type start_time: datetime.datetime or str
		:param end_time:
		:type end_time: datetime.datetime or str
		:param criteria: An already existing list with critria, to which we will append the time criteria
		:type criteria: list
		:return: The new list of criteria
		:rtype: list
		"""
		if criteria is None:
			criteria = []
		if start_time is not None:
			t_start = start_time.strftime(self.DT_FORMAT) if isinstance(start_time, dt) else start_time
			criteria.append(('time', '>=', t_start))
		if end_time is not None:
			t_end = end_time.strftime(self.DT_FORMAT) if isinstance(end_time, dt) else end_time
			criteria.append(('time', '<=', t_end))
		return criteria

	def getMeasurementRecordsByPeriod(self, channel, start_time=None, end_time=None, criteria=None, async=None):
		result = []
		try:
			if criteria is None:
				criteria = {}
			if start_time is None:
				start_time = self.getTimeZero()
			if end_time is None:
				end_time = dt.now()
			t_points = self.getDateTimeSequence(t_start=start_time, t_end=end_time)
			for t_point in t_points:
				# if all the records are from a different day; we don't need the end_time
				if isinstance(t_point, dt):
					t_point = t_point.date()
				if t_point > start_time.date():
					start_time = None
				if t_point < end_time.date():
					end_time = None
				# create new criteria
				criteria = self.createPeriodCriteria(start_time=start_time, end_time=end_time, criteria=criteria)
				# get  records
				records = self.getMeasurementsByDate(
					channel, date=t_point, criteria=self.parseCriteria(*criteria), async=async
				)
				result.extend(records)
		except pyorient.PyOrientException as poe:
			self.getLog().critical("Unable to get Measurements:\n{}".format(poe))
		return result

	def getDateTimeSequence(self, t_start, t_end=None, by="days"):
		"""

		:param t_start:
		:type t_start: datetime.datetime
		:param t_end:
		:type t_end:  datetime.datetime
		:param by:
		:type by:
		:return:
		:rtype: list[datetime.datetime]
		"""
		if by not in ["days", "month", "year"]:
			raise ValueError("Invalid value received: by = {}".format(by))
		if t_end is None:
			t_end = dt.now()
		n_days = (t_end.date()- t_start.date()).days
		result = [(t_start + td(**{by: x})).date() for x in range(0, n_days + 1)]
		return result

	def getMeasurementsByDate(self, channel, date, criteria=None, async=None):
		result = []
		try:
			if criteria is None:
				criteria = []
			where_clause = ""
			if len(criteria) > 0:
				where_clause = " where {}".format(" and ".join(criteria))
			year, month, day = self.getDateRecords(channel, date)
			if None  in (year, month, day):
				raise ValueError("[getMeasurementsByDate] None in Year/Month/Day Records")
			query = "select from (select expand(out(\"has_measurement\")[@class=\"mcMeasurement\"]) " \
			        "from {source}){where}".format(
				source=day._rid,
				where=where_clause
			)
			if async is None:
				result = self.getClient().query(query, -1, "0:0")
			else:
				self.getClient().query_async(query, -1, "0:0", async)
				result = []
		except Exception as e:
			self.getLog().critical("[getMeasurementsByDate] Exception raised: \n{}".format(e))
		return result

	def getMeasurementRecords(self, channel, time_point, criteria=None, async=None):
		result = []
		try:
			if criteria is None:
				criteria = []
			where = ""
			# add time point
			criteria.append(('time', "=", time_point))
			if len(criteria) > 0:
				where = " where {}".format(" and ".join(self.parseCriteria(*criteria)))
			year, month, day = self.getDateRecords(channel, time_point)
			if None in (year, month, day):
				raise ValueError("[getMeasurementRecords] None in Year/Month/Day Records")
			query = "select FROM (select expand(out('has_measurement')) from {source}){where}".format(
				source=day._rid, where=where
			)
			if async is None:
				result = self.getClient().query(query, -1, "0:0")
			else:
				self.getClient().query_async(query, -1, "0:0", async)
				result = []
		except pyorient.PyOrientException as poe:
			self.getLog().critical("Unable to get measurement record: \n {}".format(
				poe
			))
		return result

	def getMeasurementRecord(self, channel, time_point, criteria=None, async=None):
		"""
		Retrieves the Measurement that is me
		:param channel:
		:type channel: pyorient.types.OrientRecord
		:param time_point:
		:type time_point: datetime.datetime
		:return:
		:rtype: None or pyorient.types.OrientRecords
		"""
		result = None
		records = self.getMeasurementRecords(channel=channel, time_point=time_point, criteria=criteria, async=async)
		if len(records) > 0:
			result = records[0]
		return result

	def createMeasurementRecord(self, channel, measurement):
		"""

		:param measurement:
		:type measurement: uvaCultivator.uvaODMeasurement.ODMeasurement
		:return:
		:rtype:
		"""
		result = None
		try:
			result = self.createVertex(rClass="mcMeasurement", **self.parseToDict(measurement))
		except pyorient.PyOrientException as poe:
			self.getLog().critical("Unable to create Measurement record:\n{}".format(poe))
		if result is not None:
			# get date record
			year, month, day = self.createDateRecords(channel, measurement.getTimePoint())
			# connect measurement to day record
			self.createEdge(day._rid, result._rid, "has_measurement")
		return result

	def createEdge(self, from_rid, to_rid, name ):
		"""
		Creates an edge between two Nodes and will autmatically set the in and out properties of the edge.
		:param from_rid: Record ID of the Nodes from where the edge will be created
		:type from_rid: int
		:param to_rid: Record ID of the Nodes to where the edge will be created
		:type to_rid: int
		:param name: Name of the edge
		:type name: str
		:return: Orient Record of the Edge or None if it failed
		:rtype: pyorient.types.OrientRecord or None
		"""
		result = None
		try:
			query =  "create edge {name} from {fRID} to {tRID} SET out={fRID}, in={tRID}".format(
				name=name, fRID=from_rid, tRID=to_rid
			)
			result = self.getClient().command(query)
			result = result[0] if len(result) > 0 else None
		except pyorient.PyOrientCommandException as pce:
			self.getLog().critical("Unable to create edge {name} from {fRID} to {tRID}:\n{e}".format(
				name=name, fRID=from_rid, tRID=to_rid, e=pce))
		return result

	def createVertex(self, rClass="V", **properties ):
		"""
		Creates a Vertex record in the OrientDB.
		:param rClass: The class name, defaults to the standard vertex
		:type rClass: str
		:param properties: Dictionary of properties given as kwargs
		:type properties: dict
		:return: Returns the created record or None if it failed to created one.s
		:rtype: pyorient.types.OrientRecord or None
		"""
		result = False
		try:
			rec = { '@{}'.format(rClass): properties }
			cluster = self.getCluster(rClass)
			if cluster is not None:
				result = self.getClient().record_create(cluster, rec)
		#	result = result[0] if len(result) > 0 else None
		except pyorient.PyOrientCommandException as pce:
			self.getLog().critical("Unable to create Vertex: \n{} \n RECORD: \n".format(pce, rec))
		return result

	def getDateRecords(self, channel, date):
		"""
		Returns the records (Year / month / date) that form the date
		:param channel: The channel to which the date should be connected
		:type channel: pyorient.types.OrientRecord
		:param date: The date to find
		:type date: datetime.datetime
		:return: Returns a list of size 3 with the found records or None if there were records missing.
		:rtype: list[ pyorient.types.OrientRecord or None]
		"""
		result = [None, None, None]
		try:
			result[0] = self.getYearRecord(channel, date)
			if result[0] is None:
				raise Exception("There is no Year {} Record connected to {}".format(date.year, channel._rid))
			result[1] = self.getMonthRecord(result[0], date)
			if result[1] is None:
				raise Exception("There is no Month {} Record connected to {}".format(date.month, channel._rid))
			result[2] = self.getDayRecord(result[1], date)
			if result[2] is None:
				raise Exception("There is no Day {} Record connected to {}".format(date.day, channel._rid))
		except Exception as e:
			self.getLog().info("Unable to retrieve the correct Date records: {}".format(
				e)
			)
		return result

	def hasDateRecords(self, channel, date):
		"""
		Determines whether the date records (year, month , date) exist in a relation with the channel
		:param channel:
		:type channel: pyorient.types.OrientRecord
		:param date:
		:type date: datetime.datetime
		:return:
		:rtype:
		"""
		# has year?
		result = self.hasYearRecord(channel, date)
		if result is True:
			year = self.getYearRecord(channel, date)
			result = self.hasMonthRecord(year, date)
			if result is True:
				month = self.getMonthRecord(year, date)
				result = self.hasDayRecord(month, date)
		return result

	def createDateRecords(self, channel, date):
		"""
		Creates and connects the records that form the date (Year / month / day)
		:param channel:
		:type channel: pyorient.types.OrientRecord
		:param date:
		:type date: datetime.datetime
		:return: list with three Records: Year, Month and Day
		:rtype: list[pyorient.types.OrientRecord]
		"""
		year = self.getYearRecord(channel, date)
		if year is None:
			self.getLog().info("Create a Year Record for Channel {} with value = {}".format(
				channel._rid, date.year
			))
			year = self.createYearRecord(channel, date)
		month = self.getMonthRecord(year, date)
		if month is None:
			self.getLog().info("Create a Month Record for Channel {} via {} with value = {}".format(
				channel._rid, year._rid, date.month
			))
			month = self.createMonthRecord(year, date)
		day = self.getDayRecord(month, date)
		if day is None:
			self.getLog().info("Create a Day Record for Channel {} via {} with value = {}".format(
				channel._rid, month._rid, date.day
			))
			day = self.createDayRecord(month, date)
		return [ year, month, day]

	def getRecords(self, selector, source, criteria=None):
		"""
		Returns all the records that match the criteria and are associated with V
		:param source:
		:type source: pyorient.types.OrientRecord
		:param criteria: A list of tuples of three values: LHS COMP RHS ( x > y | x = y, etc.)
		:type criteria: list[(str, str, str)]
		:return:
		:rtype:
		"""
		result = []
		query = ""
		try:
			if criteria is not None and len(criteria) > 0:
				clauses = [ "".join(clause) for clause in criteria ]
				where = "{}".format( " and ".join(clauses) )
			query =	"select {selector} FROM {source}{where}".format(
				selector=selector,
				source=source,
				where=where
			)
			result = self.getClient().query(query)
		except pyorient.PyOrientException as poe:
			self.getLog().critical("Unable to run\n\t\t {}\n\t See:\n\t\t {}".format(query, poe))
		return result

	def getYearRecord(self, channel, date):
		"""
		Returns the year record of the given date that is connected to the channel
		:param channel:
		:type channel: pyorient.types.OrientRecord
		:param date:
		:type date: datetime.datetime
		:return:
		:rtype: pyorient.types.OrientRecord or None
		"""
		result = None
		try:
			query = "select expand(out(\"has_year\")[value={year}]) from {source}".format(
				year=date.year, source=channel._rid
			)
			result = self.getClient().query(query)
			result = result[0] if len(result) > 0 else None
		except pyorient.PyOrientException as poe:
			self.getLog().critical("Unable to get Year Record: {}".format(poe))
		return result

	def hasYearRecord(self, channel, date):
		"""
		Returns if a Year Record with the year according to date is connected to the channel
		:param channel:
		:type channel: pyorient.types.OrientRecord
		:param date:
		:type date: datetime.datetime
		:return:
		:rtype: pyorient.types.OrientRecord or None
		"""
		return self.getYearRecord(channel=channel, date=date) is not None

	def createYearRecord(self, channel, date):
		"""
		Creates and connect a year record to the channel, with year from date
		:param channel:
		:type channel: pyorient.types.OrientRecord
		:param date:
		:type date: datetime.datetime
		:return:
		:rtype: pyorient.types.OrientRecord or None
		"""
		result = None
		try:
			year = self.createVertex('Year', value=date.year, size=0)
			if year is not None:
			# connect to channel
				result = self.createEdge(from_rid=channel._rid, to_rid=year._rid, name="has_year")
				# if success (result is not None) set year as result
				if result is not None:
					result = year
		except pyorient.PyOrientException as poe:
			self.getLog().critical("Unable to create Year Record: {}".format(poe))
		return result

	def getMonthRecord(self, year, date):
		"""
		Returns the Month Record according to the date and that's connected to the channel
		:param channel:
		:type channel: pyorient.types.OrientRecord
		:param date:
		:type date: datetime.datetime
		:return:
		:rtype: pyorient.types.OrientRecord or None
		"""
		result = None
		try:
			query = "select expand(out(\"has_month\")[value={value}]) from {source}".format(
				value=date.month, source=year._rid
			)
			result = self.getClient().query(query)
			result = result[0] if len(result) > 0 else None
		except pyorient.PyOrientException as poe:
			self.getLog().critical("Unable to get Month Record: {}".format(poe))
		return result

	def hasMonthRecord(self, year, date):
		"""
		Returns if a Month of the month according the date is connected to the selected year
		:param year:
		:type year: pyorient.types.OrientRecord
		:param date:
		:type date: datetime.datetime
		:return:
		:rtype: pyorient.types.OrientRecord or None
		"""
		return self.getMonthRecord(year=year, date=date) is not None

	def createMonthRecord(self, year, date):
		"""
		Creates a Month Record and connects it to the Year record according to the date object
		:param year:
		:type year: pyorient.types.OrientRecord
		:param date:
		:type date: datetime.datetime
		:return:
		:rtype: pyorient.types.OrientRecord or None
		"""
		result = None
		try:
			month = self.createVertex('Month', value=date.month, size=0)
			if month is not None:
				# connect to channel
				result = self.createEdge(from_rid=year._rid, to_rid=month._rid, name="has_month")
				# if success (result is not None) set year as result
				if result is not None:
					result = month
		except pyorient.PyOrientException as poe:
			self.getLog().critical("Unable to create Month Record: {}".format(poe))
		return result

	def getDayRecord(self, month, date):
		"""
		Returns the Day Record according to the date and that's connected to the month
		:param month:
		:type month: pyorient.types.OrientRecord
		:param date:
		:type date: datetime.datetime
		:return:
		:rtype: pyorient.types.OrientRecord or None
		"""
		result = None
		try:
			query = "select expand(out(\"has_day\")[value={value}]) from {source}".format(
				value=date.day, source=month._rid
			)
			result = self.getClient().query(query)
			result = result[0] if len(result) > 0 else None
		except pyorient.PyOrientException as poe:
			self.getLog().critical("Unable to get Day Record: {}".format(poe))
		return result

	def hasDayRecord(self, month, date):
		"""
		Returns if a Month of the month according the date is connected to the selected year
		:param month:
		:type mont: pyorient.types.OrientRecord
		:param date:
		:type date: datetime.datetime
		:return:
		:rtype: pyorient.types.OrientRecord or None
		"""
		return self.getDayRecord(month=month, date=date) is not None

	def createDayRecord(self, month, date):
		"""
		Creates a Day Record and connects it to the Month record according to the date object
		:param month:
		:type month: pyorient.types.OrientRecord
		:param date:
		:type date: datetime.datetime
		:return:
		:rtype: pyorient.types.OrientRecord or None
		"""
		result = None
		try:
			day = self.createVertex('Day', value=date.day, size=0)
			if day is not None:
				# connect to channel
				result = self.createEdge(from_rid=month._rid, to_rid=day._rid, name="has_day")
				# if success (result is not None) set year as result
				if result is not None:
					result = day
		except pyorient.PyOrientException as poe:
			self.getLog().critical("Unable to create Day Record: {}".format(poe))
		return result

	def getProjectId(self):
		"""
		Return the currently used project_id
		:rtype: str
		"""
		return self.retrieveSetting(name="project_id", default=self._default_settings)

	def getTimeZero(self):
		"""
		Return the Time Zero of the currently used project id
		:rtype: datetime.datetime
		"""
		return self.retrieveSetting(name="time_zero", default=self._default_settings)

	def getState(self):
		return self.retrieveSetting(name="state", default=self._default_settings)

	def setTimeZero(self, t_zero, write=False):
		"""
		Set the time zero of the currently used project id
		:param t_zero: The new t_zero
		:type t_zero: datetime.datetime
		:param write: Whether to store this change to the database
		:type write: bool
		:rtype: SimpleMongoDBHandler
		"""
		result = True
		if write:
			result = ['0']
			e_record = self.getExperiment()
			try:
				result = self.getClient().command("update {experiment} set t_zero = {t_zero}".format(
					experiment=e_record._rid, t_zero=t_zero.strftime(self.DT_FORMAT)
				))
			except pyorient.PyOrientException as poe:
				self.getLog().critical("Unable to set Time Zero:\n{}".format(poe))
			result = len(result) > 0 and result[0] == '1'
		if result:
			self._settings["time_zero"] = t_zero
		return self.getTimeZero()

	def getODMeasurement(self, channel, time_point):
		result = None
		m_record = self.getMeasurementRecord(channel=channel, time_point=time_point)
		if m_record is not None:
			result = self.parseToODMeasurement(m_record)
		return result

	def parseToDict(self, measurement):
		"""
		Parse a OD Measurement to JSON format
		:param measurement:
		:type measurement: uvaCultivator.uvaODMeasurement.ODMeasurement
		:return:
		:rtype:
		"""
		# """
		# create class mcMeasurement extends V
		# 		create property mcMeasurement.intensity float
		# 		create property mcMeasurement.background float
		# 		create property mcMeasurement.flash float
		# 		create property mcMeasurement.od_led integer
		# 		create property mcMeasurement.od_repeats integer
		# 		create property mcMeasurement.od_value float
		# 		create property mcMeasurement.od_raw float
		# 		create property mcMeasurement.temperature float
		# """
		return {
			'time': measurement.getTimePoint(),
			'intensity': measurement.getIntensity(),
			'background': measurement.getBackground(),
			'flash': measurement.getFlash(),
			'od_led': measurement.getLED(asIndex=False),
			'od_repeats': measurement.getRepeats(),
			'od_value': measurement.getODValue(),
			'od_raw': measurement.getODRaw(),
			'temperature': measurement.getTemperature()
		}


	def parseToODMeasurement(self, record):
		result = None
		if record._class.lower() in ["measurement".lower(), "mcmeasurement".lower()]:
			attributes = {
				't_point': record.time,
				't_zero': self.getTimeZero(),
				'od_value': record.od_value,
				'od_raw': record.od_raw,
				'intensity': record.intensity,
				'background': record.background,
				'flash': record.flash,
				'od_repeats': record.od_repeats,
				'od_led': record.od_led,
				'temperature': record.temperature,
			}
			result = odM(**attributes)
		return result

	def emptyCache(self):
		self._data_cache.clear()

	def readODMeasurements(self, start_time=None, end_time=None, **criteria):
		"""
		Read all measurements and return them as ODMeasurement Objects
		:return: dict of ODMeasurement Objects { channel : {time : [m]}}
		:rtype: dict[ int, dict[ datetime.datetime, list[uvaCultivator.uvaODMeasurement.ODMeasurement]]]
		"""
		result = {}
		if not self.isConnected():
			self.connect()
		e_record = self.getExperiment(force=True)
		if self.hasExperiment():
			channels = self.getChannelRecords(e_record) # get all channel records
			# remove old measurements
			self.emptyCache()
			for channel in channels:
				# load measurements a_sync
				records = self.getMeasurementRecordsByPeriod(
					channel=channel, start_time=start_time, end_time=end_time, criteria=self.makeCriteria(**criteria)
				)
				result.update(self.processODMeasurements(deque(records), channel=channel))
		return result

	def processODMeasurements(self, records=None, channel=None):
		result = {}
		# process incoming measurements
		if records is None:
			records = self._data_cache
		cache_length = len(records)
		while len(records) > 0:
			record = records.popleft()  # get the oldest
			if channel is None:
				channel = self.getChannelRecordFromMeasurement(record)
			m = self.parseToODMeasurement(record=record)
			if None not in [channel, m]:
				if channel.channel_id in result.keys():
					if m.getTimePoint() in result[channel.channel_id].keys():
						result[channel.channel_id][m.getTimePoint()].append(m)
					else:
						result[channel.channel_id][m.getTimePoint()] = [m]
				else:
					result[channel.channel_id] = {m.getTimePoint(): [m]}
				# all measurements processed
		return result

	def addODMeasurement(self, measurement, channel_id):
		result = None
		# check channels
		if not self.hasChannelRecord(self.getExperiment(), channel_id=channel_id):
			self.getLog().info("[addODMeasurement] Channel record not found, create a new record")
			self.createChannelRecord( self.getExperiment(), channel_id=channel_id)
		# check for duplicate measurement
		channel = self.getChannelRecord(self.getExperiment(), channel_id=channel_id)
		if channel is not None:
			# TODO : duplicate check
			result = self.createMeasurementRecord(measurement=measurement, channel=channel)
		return result is not None

	def addODMeasurements(self, measurements):
		"""Export a dictionary of new Measurements grouped by channel

		:param measurements: Dictionary of channels with lists of OD Measurements to add { channel : [m] }
		:type measurements: dict[int, list[uvaCultivator.uvaODMeasurement.ODMeasurement]]
		:rtype: bool
		"""
		result = False
		# first connect
		try:
			if not self.isConnected():
				self.getLog().info("[addODMeasurements] Not connected yet, connect now")
				self.connect()
			if not self.isConnected():
				raise Exception("Cannot connect to database at {}".format(self.getDatabaseURL()))
			# make sure we have the class defined in the DB
			self.createClasses()
			# load the experiment
			self._experiment = self.getExperiment(force=True)
			if not self.hasExperiment():
				self.getLog().info("[addODMeasurements] No experiment record found, create new record")
				self.createExperimentRecord(self.getProjectId(), self.getTimeZero(), self.getState())
				self._experiment = self.getExperiment(force=True)
			self.getLog().debug("[addODMeasurements] Experiment = {}, criteria: {} / {}".format(
				self.getExperiment()._rid,
				self.getProjectId(),
				self.getTimeZero()
			))
			if self.hasExperiment():
				result = True
				for channel in measurements.keys():
					for measurement in measurements[channel]:
						result = self.addODMeasurement(measurement, channel) and result
				# end for measurement
			# end for channel
		except Exception as e:
			self.getLog().critical("Unable to add Measurement:\n{}".format(e))
		return result
