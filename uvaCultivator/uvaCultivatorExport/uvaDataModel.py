# coding=utf-8
"""
This module provides the base classes that are used to describe a serie of measurements in a experiment.
Therefore the module provides 2 classes (experiment and measurement) and 3 subclasses of the measurement class.

"""

__author__ = 'Joeri Jongbloets <j.a.jongbloets@student.vu.nl>'

from ..uvaCultivator import *


class uvaExperiment(uvaObject):
	"""
	Base class describing an Experiment object
	"""

	_default_settings = uvaObject.mergeDictionary(
		uvaObject._default_settings,
		{
			# TODO : Set some settings
		})

	def __init__(self, project_id, t_zero, t_end=None, state=True, settings=None):
		uvaObject.__init__(self, settings=settings)
		# Store the data as a dictionary of channels each containing a dictionary with time points as key that point
		# to a list of measurements that are performed at that moment
		self._data = {} # { channel : { time_point : [measurement] }
		""":type: dict[int, dict[ datetime.datetime, list[uvaMeasurement]]]"""
		self._project_id = project_id
		self._t_zero = t_zero
		self._t_end = t_end
		self._state = state

	def getTimeZero(self):
		"""
		:rtype: datetime.datetime
		"""
		return self._t_zero

	def getTimeEnd(self):
		"""
		:rtype: datetime.datetime
		"""
		return self._t_end

	def setState(self, state):
		"""
		If set to False, the experiment is always regarded as inactive, when set to True, the t_zero and t_end
		determine the state.
		:param state:
		:type state: bool
		:rtype: Experiment
		"""
		self._state = state is True
		return self

	def isActive(self):
		"""
		Checks whether the state of this experiment is set to True and checks the End date. When the end_date is None
		it is assumed that this project will run indefinitely, otherwise the end date should be in the future or now.
		The project is also not active when the t_zero lies in the future.
		:rtype: bool
		"""
		return self._state and (self._t_end is None or self._t_end >= dt.now() >= self._t_zero)

	def getProjectId(self):
		return self._project_id

	def hasChannel(self, channel):
		"""
		Returns whether the channel is present as key in the data dictionary.
		:param channel: The channel to look for
		:type channel: int
		"""
		return channel is not None and channel in self._data.keys()

	def hasTimepoint(self, time_point, channel=None):
		"""
		Returns whether a channel (if channel is None) or the channel (if channel is specified) has the timepoint as
		key in the data dictionary
		:type time_point: datetime.datetime
		:type channel: int or None
		:return return a list of channels (or channel if specified) that have the timepoint
		:rtype: list[int]
		"""
		result = []
		channels = [channel if self.hasChannel(channel) else None]
		if channel is None:
			channels = self._data.keys()
		for channel in channels:
			if time_point in self._data[channel].keys():
				result.append(channel)
		return result

	def findMeasurement(self, measurement, time_point, channel=None):
		"""
		Searches for a similar (or equal) measurement object at the given time point and channel and returns the
		found object or None when no equal measurement was found.
		:param measurement:
		:type measurement:
		:param time_point:
		:type time_point:
		:param channel:
		:type channel:
		:return: The measurement that is equal or None
		:rtype: uvaMeasurement or None
		"""
		result = None
		channels = [channel if self.hasChannel(channel) else self._data.keys()]
		for channel in channels:
			if self.hasTimepoint(time_point=time_point, channel=channel):
				measurements = self.getMeasurements(time_point=time_point, channel=channel)
				for m in measurements:
					if measurement == m:
						result = m
						break
		return result


	def getMeasurements(self, time_point, channel):
		"""
		Returns a list of measurements that are recorded at the given channel and time point. Returns an empty list
		if either the channel or time point are not present in the data set.
		:param time_point: Time point at which the measurement was made
		:type time_point: datetime.datetime
		:param channel: Channel in the multicultivator at which the measurement was made
		:type channel: int
		:return: List of measuremens made at the time point and channel
		:rtype: list
		"""
		result = []
		if len(self.hasTimepoint(channel=channel, time_point=time_point)) > 0:
			result = self._data[channel][time_point]
		return result

	def getMeasurementsByChannel(self, channel):
		"""
		Returns all measurements performed at this channel
		:type channel: int
		:rtype: dict
		"""
		result = {}
		if self.hasChannel(channel):
			result = self._data[channel]
		return result

	def getMeasurementsByPeriod(self, t_start, t_end=None, channel=None):
		"""
		Returns all measurements that are performed between t_start and t_end (including the end points) and if
		channel is specified
		:type t_start: datetime.datetime
		:type t_end: datetime.datetime or None
		:type channel: int or None
		:rtype: dict[int, dict[ datetime.datetime, list[uvaMeasurement[T<=uvaMeasuremnt]]]]
		"""
		result = {}
		channels = [channel if self.hasChannel(channel) else self._data.keys()]
		for channel in channels:
			for t_now in self._data[channel].keys():
				if t_start <= t_now and (t_end >= t_now or t_end is None):
					if channel not in result:
						result[channel] = {}
					else:
						result[channel][t_now] = self._data[channel][t_now]
					# end of if channel in result
				# end of if t_start
			# end of for t_now
		# end of for channel
		return result

	def getMeasurementsByTimePoint(self, time_point, channel=None):
		result = {}
		channels = self.hasTimepoint(time_point, channel)
		for channel in channels:
			result[channel] = { time_point : self._data[channel][time_point]}
		return result

	def addMeasurement(self, time_point, channel, measurement):
		"""
		Will add a measurement to a channel at a specific time point. This will preserve existing measurements and
		treat the new measurement as an additional one.
		"""
		if self.hasChannel(channel):
			if self.hasTimepoint(time_point=time_point, channel=channel):
				# time point already exists -> append measurement
				self._data[channel][time_point].append(measurement)
			else:
				# time point does not exist -> create new list
				self._data[channel][time_point] = [measurement]
		else:
			# add channel
			self._data[channel] = {time_point: [measurement]}
		return measurement in self._data[channel][time_point]

	def setMeasurement(self, time_point, channel, measurement):
		"""
		Will add or replace a measurement at a channel at a specific time point. This will not preserve existing
		measurements of the same class (!).
		"""
		if self.hasChannel(channel):
			if self.hasTimepoint(time_point=time_point, channel=channel):
				# time point exists: iterate over measurement
				measurements = self.getMeasurements(time_point=time_point, channel=channel)
				is_present = False
				for idx, m in enumerate(measurements):
					is_present = isinstance(m, measurement.__class__)
					if is_present:
						# replace!
						self._data[channel][time_point][idx] = measurement
						break
				# end of for
				if not is_present:
					# add it
					self._data[channel][time_point].append(measurement)
			else:
				# add time point
				self._data[channel][time_point] = [measurement]
		else:
			# add channel and timepoint
			self._data[channel] = {time_point: [measurement]}
		return measurement in self._data[channel][time_point]


	def sync(self):
		pass

	def load(self, handler=None, settings=None):
		pass

	def save(self, handler=None, settings=None):
		pass

class uvaMeasurement(uvaObject):
	"""
	Basic class that supports the storage and analysis of a data point. Implementations could implement the support
	for OD time points or temperature time points.
	"""

	_default_settings = uvaObject.mergeDictionary(
		uvaObject._default_settings,
		{
			# TODO : Set some settings
		})

	def __init__(self, settings):
		uvaObject.__init__(self, settings=settings)
		self._data = {}
		self._t_now = None
		self._channel = None

	def load(self, handler=None, settings=None):
		pass

	def save(self, handler=None, settings=None):
		pass

	def getTimePoint(self):
		return self._t_now

	def setTimePoint(self, time_point):
		self._t_now = time_point

	def getChannel(self):
		return self._channel

	def setChannel(self, channel):
		self._channel = channel

	def getReading(self, name):
		result = None
		if name in self._data.keys():
			result = self._data[name]
		return result

	def setReading(self, name, value):
		result = False
		if name in self._data.keys():
			self._data[name] = value
			result = True
		return result

	def __eq__(self, other):
		"""
		Compares two uvaMeasurement objects
		:param other:
		:type other: uvaMeasurement
		:return:
		:rtype:
		"""
		result = isinstance(other, self.__class__) and self._t_now == other.getTimePoint() \
						and self._channel == other.getChannel()
		if result:
			# check readings
			for reading in self._data.keys():
				result = result and self._data[reading] == other.getReading(reading)
				if not result:
					break
		return result

	@classmethod
	def findMeasurement(cls, **attributes):
		pass

	@classmethod
	def createMeasurement(cls, time_point, channel, experiment=None, settings=None, **readings):
		"""
		Creates a new measurement object. Will check the experiment object for duplicates and if it finds a duplicate
		this object is return instead of creating a new object.
		:param time_point: Time point at which the measurement was made
		:type time_point: datetime.datetime
		:param channel: Channel in the multicultivator at which the measurement was made
		:type channel: int
		:param experiment: Optional Experiment object, the created Measurement is automatically added to the experiment
		:type experiment: uvaExperiment or None
		:param settings: Dictionary with settings, for compatibility with uvaObject
		:type settings: dict
		:return:
		:rtype:
		"""
		result = cls(settings)
		result.setTimePoint(time_point)
		result.setChannel(channel)
		if experiment is not None:
				duplicate = experiment.findMeasurement(result, time_point=time_point, channel=channel)
				if duplicate is not None:
					result = duplicate
		return result

class uvaODMeasurement(uvaMeasurement):

	_default_settings = uvaMeasurement.mergeDictionary(
		uvaMeasurement._default_settings,
		{  # TODO : Set some settings
		})

	def __init__(self, settings=None):
		uvaMeasurement.__init__(self, settings=settings)

	@classmethod
	def createMeasurement(cls, time_point, channel, experiment=None, settings=None, **readings):
		pass

class uvaLightMeasurement(uvaMeasurement):

	_default_settings = uvaMeasurement.mergeDictionary(
		uvaMeasurement._default_settings,
		{  # TODO : Set some settings
		})

	def __init__(self, settings=None):
		uvaMeasurement.__init__(self, settings=settings)

	@classmethod
	def createMeasurement(cls, time_point, channel, experiment=None, settings=None, **readings):
		result = cls(settings)
		result.setTimePoint(time_point)
		result.setChannel(channel)

		if experiment is not None:
			duplicate = experiment.findMeasurement(result, time_point=time_point, channel=channel)
			if duplicate is not None:
				result = duplicate
		return result


class uvaTemperatureMeasurement(uvaMeasurement):

	_default_settings = uvaMeasurement.mergeDictionary(
		uvaMeasurement._default_settings,
		{  # TODO : Set some settings
		})

	def __init__(self, settings=None):
		uvaMeasurement.__init__(self, settings=settings)

	@classmethod
	def createMeasurement(cls, time_point, channel, experiment=None, settings=None, **readings):
		result = cls(settings)
		result.setTimePoint(time_point)
		result.setChannel(channel)

		if experiment is not None:
			duplicate = experiment.findMeasurement(result, time_point=time_point, channel=channel)
			if duplicate is not None:
				result = duplicate
		return result
