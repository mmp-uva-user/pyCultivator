# coding=utf-8
"""
"""

__author__ = 'Joeri Jongbloets <j.a.jongbloets@student.vu.nl>'

from datetime import datetime as dt, timedelta as td
from ming import Session, schema
from ming.odm import FieldProperty, ForeignIdProperty, RelationProperty
from ming.odm import ThreadLocalODMSession, Mapper
from ming.odm.declarative import MappedClass

doc_session = Session.by_name('cultivator')
session = ThreadLocalODMSession(doc_session=doc_session)


class Experiment(MappedClass):

	class __mongometa__:
		session = session
		name = 'experiment'

	_id = FieldProperty(schema.ObjectId)
	_project_id = FieldProperty(str)
	_t_zero = FieldProperty(dt, if_missing=dt.now)
	_t_end = FieldProperty(dt, if_missing=None)
	_active = FieldProperty(bool, if_missing=False)
	_measurements = RelationProperty('Measurement')

	def __init__(self, project_id, t_zero=None, active=True):
		super(Experiment, self).__init__()
		if t_zero is None:
			t_zero = dt.now()
		self._t_zero = t_zero
		self._project_id = project_id
		self._active = active

	def getMeasurements(self):
		return list(self._measurements)

	def getTimeZero(self):
		"""
		:rtype: datetime.datetime
		"""
		return self._t_zero

	def getTimeEnd(self):
		"""
		:rtype: datetime.datetime
		"""
		return self._t_end

	def setState(self, state):
		"""
		If set to False, the experiment is always regarded as inactive, when set to True, the t_zero and t_end
		determine the state.
		:param state:
		:type state: bool
		:rtype: Experiment
		"""
		self._active = state is True
		return self

	def isActive(self):
		"""
		Checks whether the state of this experiment is set to True and checks the End date. When the end_date is None
		it is assumed that this project will run indefinitely, otherwise the end date should be in the future or now.
		The project is also not active when the t_zero lies in the future.
		:rtype: bool
		"""
		return self._active and (self._t_end is None or self._t_end >= dt.now() >= self._t_zero)

	def getProjectId(self):
		return self._project_id

	def getId(self):
		return self._id

	@classmethod
	def retrieveById(cls, id):
		return cls.query.get({"_id": id})

	@classmethod
	def retrieveByAttributes(cls, project_id, t_zero=None):
		"""

		:param project_id:
		:type project_id: int
		:param t_zero:
		:type t_zero: datetime.datetime
		:return:
		:rtype: list
		"""
		return cls.findByAttributes(project_id=project_id, time_zero=t_zero).all()

	@classmethod
	def findByAttributes(cls, project_id=None, time_zero=None, time_end=None, active=None):
		"""
		Find all experiments (or it's subclasses) that satisfy the given criteria. A criteria is omitted when its
		value is set to None. If time_end is included as criteria, all None values are automatically accepted
		:param project_id:
		:type project_id: int
		:param time_zero:
		:type time_zero: datetime.datetime
		:return:
		:rtype: ming.base.Cursor
		"""
		attributes = []
		if project_id is not None:
			attributes.append({"_project_id": project_id})
		if time_zero is not None:
			attributes.append({"_t_zero": time_zero})
		if active is not None:
			attributes.append({"_active": active})
		if time_end is not None:
			# accept None as being indefinite
			attributes.append({"_t_end": time_end})

		if len(attributes) > 1:
			result = cls.query.find({"$and": attributes})
		elif len(attributes) == 1:
			result = cls.query.find(attributes[0])
		else:
			result = cls.query.find()
		return result

	@classmethod
	def exists(cls, project_id, t_zero=None, t_end=None):
		"""
		Searches the database for an experiment described with the given criteria. A experiment is considered a
		duplicate if the given t_zero is included in its active state period (so t_zero_dup <= t_zero <= t_end <=
		t_end_dup)
		:param project_id:
		:type project_id: int
		:param t_zero:
		:type t_zero: datetime.datetime
		:type t_end: datetime.datetime
		:return:
		:rtype: bool
		"""
		# only use time selection if t_zero and t_end are given and they are in the correct order (t_end is after
		# t_zero).
		result = False
		if None not in [t_zero, t_end] and t_end >= t_zero:
			query = {"$and": [{"project_id": project_id}]}
			query["$and"].append({"_t_zero": {"$lte": t_zero}})
			query["$and"].append({"$or": [{"_t_end": None}, {"_t_end": {"$gte": t_end}} ]})
			result = cls.query.find(query).count() > 0
		else:
			result = cls.query.find({"_project_id": project_id})
		return result

	@classmethod
	def loadExperiment(cls, project_id, t_zero=None, state=False):
		result = Experiment.findByAttributes(project_id, t_zero)
		if result.count() > 0:
			result = result.first()
		else:
			result = Experiment(project_id=project_id, t_zero=t_zero)
			session.flush()
		return result


class Measurement(MappedClass):
	class __mongometa__:
		session = session
		name = 'measurement'
		polymorphic_on = '_type'
		polymorphic_identity = 'measurement'

	_id = FieldProperty(schema.ObjectId)
	_type = FieldProperty(str, if_missing='measurement')
	_t_point = FieldProperty(dt, if_missing=dt.now)
	_channel = FieldProperty(int)
	_experiment_id = ForeignIdProperty('Experiment')
	_experiment = RelationProperty('Experiment')

	def __init__(self, experiment, channel, time_point=None):
		super(Measurement, self).__init__()
		if time_point is None:
			time_point = dt.now()
		self._t_point = time_point
		self._channel = channel
		if isinstance(experiment, Experiment):
			experiment = experiment.getId()
		self._experiment_id = experiment

	@classmethod
	def getType(cls):
		return cls.__mongometa__.polymorphic_identity

	def getTime(self):
		return self._t_point

	def getChannel(self):
		return self._channel

	def getExperiment(self):
		return self._experiment

	@classmethod
	def retrieveById(cls, id):
		return cls.query.get(_id = id)

	@classmethod
	def retrieveByTime(cls, experiment, time_point, m_type=None, channel=None, max_distance=0):
		"""
		Find all Measurements that are done at the given time_point or that ly within a maximum distance of the
		desired time_point. This will return all matches.
		:param experiment:
		:param time_point:
		:type time_point: datetime.datetime
		:param max_distance: distance in seconds
		:type max_distance: int
		:return:
		:rtype: list
		"""
		time_selection = time_point
		if max_distance > 0:
			t_start = time_point - td(seconds=max_distance)
			t_end = time_point + td(seconds=max_distance)
			time_selection = {"$gte": t_start, "$lte": t_end}
		return cls.retrieveByAttributes(experiment=experiment, time_point=time_selection, m_type=m_type,
		                                channel=channel)

	@classmethod
	def retrieveByPeriod(cls, experiment, t_start, t_end, m_type=None, channel=None):
		result = []
		if isinstance(t_start, dt) and isinstance(t_end, dt):
			time_selection = {"$gte": t_start, "$lte": t_end}
			result = cls.retrieveByAttributes(experiment=experiment, time_point=time_selection, m_type=m_type,
			                                  channel=channel)
		return result

	@classmethod
	def retrieveByAttributes(cls, experiment=None, m_type=None, time_point=None, channel=None):
		"""

		:param experiment:
		:type experiment: Experiment or int or dict
		:param time_point:
		:type time_point: datetime.datetime or dict
		:param channel:
		:type channel: int or dict
		:return:
		:rtype: list
		"""
		if isinstance(experiment, Experiment):
			experiment = experiment.getId()
		return cls.findByAttributes(experiment_id=experiment, m_type=m_type, time_point=time_point,
		                            channel=channel).all()

	@classmethod
	def findByAttributes(cls, experiment_id=None, m_type=None, time_point=None, channel=None):
		"""
		Will find all Measurements (and subclasses) that satisfy the given criteria. A criteria is omitted when its
		value is set to None.
		:param experiment_id:
		:type experiment_id: int
		:param m_type: Require a certain type of measurements. If set to None; it will use the type of the class. If
		set False; the requirement is not used
		:type m_type: str, bool
		:param time_point:
		:type time_point: datetime.datetime
		:param channel:
		:type channel: int
		:return:
		:rtype: ming.base.Cursor
		"""
		attributes = []
		if m_type is None:
		# use default
			m_type = cls.getType()
		if m_type is not False:
		# if m_type is false: user asks to not include the type criterium
			attributes.append({"_type": m_type})
		if experiment_id is not None:
			attributes.append({"_experiment_id": experiment_id })
		if time_point is not None:
			attributes.append({"_t_point": time_point})
		if channel is not None:
			attributes.append({"_channel": channel})

		if len(attributes) > 1:
			result = cls.query.find({"$and": attributes})
		elif len(attributes) == 1:
			result = cls.query.find(attributes[0])
		else:
			result = cls.query.find()
		return result

	@classmethod
	def exists(cls, m):
		"""
		Determine if Measurement m already exists in the database, first search by iD then use Experiment,
		Time, Channel and type as key
		:param m:
		:type m: Measurement
		:return:
		:rtype: bool
		"""
		result = cls.retrieveById(m._id) is not None
		if result is False:
			# search by Experiment, Time, channel and type
			result = cls.findByAttributes(experiment_id=m._experiment_id, m_type=m._type, time_point=m._t_point,
			                              channel=m._channel).count() > 0
		return result

	@classmethod
	def loadMeasurement(cls, experiment, time_point, channel):
		result = None
		if isinstance(experiment, Experiment):
			experiment = experiment.getId()
		q = cls.findByAttributes(experiment_id=experiment, time_point=time_point, channel=channel)
		if q.count() > 0:
			result = q.first()
		return result


class ODMeasurement(Measurement):

	class __mongometa__:
		session = session
		polymorphic_identity = 'od_measurement'

	OD_LED_720 = 720
	OD_LED_680 = 680
	OD_LEDS = [ OD_LED_680 , OD_LED_720]

	_type = FieldProperty(str, if_missing='od_measurement')
	_od_raw = FieldProperty(float)
	_od_calibrated = FieldProperty(float, if_missing=None)
	_od_led = FieldProperty(int, if_missing=OD_LED_720)
	_od_repeats = FieldProperty(int)
	_od_flash = FieldProperty(float, if_missing=None)
	_od_background = FieldProperty(float, if_missing=None)

	def __init__(self, experiment, channel, od_raw=0, time_point=None, od_led=OD_LED_720, od_repeats=3, od_flash=0,
	             od_background=0):
		super(ODMeasurement, self).__init__(experiment=experiment, channel=channel, time_point=time_point)
		self._od_raw = od_raw
		self._od_led = od_led
		self._od_repeats = od_repeats
		self._od_flash = od_flash
		self._od_background = od_background

	def getRawOD(self):
		return self._od_raw

	def setRawOD(self, od):
		self._od_raw = od
		return self

	def getCalibratedOD(self):
		return self._od_calibrated

	def setCalibratedOD(self, od):
		self._od_calibrated = od
		return self

	def isCalibrated(self):
		return self._od_calibrated is not None

	def setODLED(self, led):
		if led == 0:
			self._od_led = self.OD_LED_680
		if led == 1:
			self._od_led = self.OD_LED_720
		return self

	def getODFlash(self):
		return self._od_flash

	def setODFlash(self, flash):
		self._od_flash = flash
		return self

	def getODBackground(self):
		return self._od_background

	def setODBackground(self, background):
		self._od_background = background
		return  self

	def getODRepeats(self):
		return self._od_repeats

	def setODRepeats(self, repeats):
		self._od_repeats = repeats
		return self

class LightMeasurement(Measurement):

	class __mongometa__:
		session = session
		polymorphic_identity = 'light_measurement'

	# Set type
	_type = FieldProperty(str, if_missing='light_measurement')
	# State of LED Panel
	_light_state = FieldProperty(bool, if_missing=None)
	# the light emitted by the vessel's LED panel
	_light_emitted = FieldProperty(float)
	# measurement of all light detected
	_light_total = FieldProperty(float, if_missing=0)
	# measurement of background light (own LED panel is OFF)
	_light_background = FieldProperty(float, if_missing=0)

	def __init__(self, experiment, channel, light_emitted=0, time_point=None, light_state=None, light_total=None,
	             light_background=None):
		super(LightMeasurement, self).__init__(experiment=experiment, channel=channel, time_point=time_point)
		self._light_emitted = light_emitted
		self._light_state = light_state
		if self._light_state is None:
			self._light_state = self._light_emitted > 0
		self._light_total = light_total
		self._light_background = light_background

	def isLightOn(self):
		return self._light_state is True

	def getLightState(self):
		return self._light_state

	def setLightState(self, state):
		self._light_state = state is True
		return self

	def getEmittedLight(self):
		return self._light_emitted

	def setEmittedLight(self, intensity):
		self._light_emitted = intensity
		return self

	def getTotalLight(self):
		return self._light_total

	def setTotalLight(self, intensity):
		self._light_total = intensity
		return self

	def getBackgroundLight(self):
		return self._light_background

	def setBackgroundLight(self, intensity):
		self._light_background = intensity
		return self

class TemperatureMeasurement(Measurement):

	class __mongometa__:
		session = session
		polymorphic_identity = 'temperature_measurement'

	_type = FieldProperty(str, if_missing='temperature_measurement')
	_temperature = FieldProperty(float)
	_temp_control = FieldProperty(bool)

	def __init__(self, experiment, channel, temperature, time_point=None, temp_control=None):
		super(TemperatureMeasurement, self).__init__(experiment=experiment, channel=channel, time_point=time_point)
		self._temperature = temperature
		self._temp_control = temp_control

	def getTemperature(self):
		return self._temperature

	def setTemperature(self, temp):
		self._temperature = temp
		return self

	def isTempControlled(self):
		return self._temp_control is True

	def setTempControl(self, control):
		self._temp_control = control is True
		return self


Mapper.compile_all()
