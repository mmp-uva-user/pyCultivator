# coding=utf-8
"""
Initialize stub for uvaCultivatorProtocol package
"""

from .. import uvaLog

__author__ = 'Joeri Jongbloets <j.a.jongbloets@student.vu.nl>'

# connect to logger
uvaLog.getLogger(__name__).debug("Connected {} to logger".format(__name__))
