# coding=utf-8
# !/usr/bin/env python
"""
Define a measurement protocol, as is a formal definition of what to do when measuring.
"""

__author__ = 'Joeri Jongbloets <j.a.jongbloets@student.vu.nl>'

from uvaCultivatorProtocol import AbstractParallelProtocol
from uvaSimpleProtocol import SimpleProtocol


class CultivationProtocol(SimpleProtocol):
    """
    This class will Measure the OD of all channels in the cultivator and
    """

    _name = "cultivation"

    def __init__(self, config_file, settings):
        super(CultivationProtocol, self).__init__(config_file=config_file, settings=settings)

    def _report(self, measurements=None):
        # self._measurements[led]{channel} = [ODMeasurement]
        # if measurements is None:
        # 	# retrieve but only select the lowest!
        # 	measurements = ( {}, {})
        # 	for led_idx in self.getCultivator().getMeasurementLeds(indices=True):
        # 		for c_idx in self.getCultivator().getChannelRange():
        # 			# return the lowest measurement but only consider the measurements from the led that we are using
        # 			m = self.getCultivator().getChannel(c_idx).getMeasurement(-1, led_idx)
        # 			measurements[led_idx][c_idx] = [m]
        if measurements is None:
            measurements = self._measurements
        return super(CultivationProtocol, self)._report(measurements=measurements)

    def _measure(self):
        # do standard measurement
        result = super(CultivationProtocol, self)._measure()
        if result:
            # adjust lights
            self.getCultivator().adjustLightsSettings(simulate=True)
            self.getCultivator().setNeighbourDependentIntensities(simulate=True)
            # apply
            for c in self.getCultivator().getChannelRange():
                if self.getCultivator().getChannel(c).hasLight():
                    self.getCultivator().getChannel(c).getLight().applyLightSettings()
        return result


class ParallelCultivationProtocol(CultivationProtocol, AbstractParallelProtocol):
    """A cultivation protocol that can be run in a separated process"""

    _name = "batch_cultivation"

    def __init__(self, config_file, settings):
        AbstractParallelProtocol.__init__(self, settings={})
        CultivationProtocol.__init__(self, config_file=config_file, settings=settings)

    # No need to override the execute function to redirect, becasue it will automatically select the Cultivation execute
    # def _measure(self):
    # 	CultivationProtocol._measure(self)
