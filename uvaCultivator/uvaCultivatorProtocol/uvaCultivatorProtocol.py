# coding=utf-8
# !/usr/bin/env python
"""
Define a measurement protocol, as is a formal definition of what to do when measuring.
"""

from .. import *
from datetime import datetime as dt
import os

__author__ = 'Joeri Jongbloets <j.a.jongbloets@uva.nl>'


class AbstractCultivatorProtocol(uvaObject):
    """Define a basic protocol class as foundation to all other protocol implementations"""

    _default_settings = uvaObject.mergeDefaultSettings(
        {
            "project_id": None,
            "time_zero": None,
            "force": False,
            "log.level": uvaLog.LEVELS["INFO"],
            "exporter.state": True,
        }
    )

    _name = "protocol_abstract"

    def __init__(self, settings=None):
        """Initialize the class"""
        super(AbstractCultivatorProtocol, self).__init__(settings=settings)
        self._cultivator = None
        self._connection = None

    @classmethod
    def getName(cls):
        """Returns the name of the protocol"""
        return cls._name

    def getProjectId(self):
        return self.retrieveSetting(name="project_id", default=self._default_settings)

    def getLogLevel(self):
        return self.retrieveSetting(name="level", default=self.getDefaultSettings(), namespace="log")

    def start(self):
        """Starts the protocol, by first preparing, then executing and at last cleaning"""
        result = self._prepare()
        if result:
            try:
                result = self._measure()
                result = result and self._react()
                if self.isReporting():
                    result = self._report()
            except KeyboardInterrupt:
                self.getLog().warning("\n\t[Protocol] Interrupted by user, quit gracefully")
            except Exception as e:
                # register error
                self.getLog().warning(
                    "\n\t[Protocol] Unexpected Error while executing protocol: {}".format(e),
                    exc_info=1
                )
        self._clean()
        return result

    @abstractmethod
    def _prepare(self):
        """Prepare for protocol execution (Config checking, connecting)

        :rtype: bool
        """
        return True

    @abstractmethod
    def _measure(self):
        """Performs the actual protocol steps

        :return: True on success, False otherwise
        :rtype: bool
        """

    def _react(self):
        """Reacts to measurements"""
        return True

    def _report(self):
        """Reports on the action"""
        return True

    @abstractmethod
    def _clean(self):
        """Clean up after protocol execution

        :rtype: bool
        """
        return True

    def forceRun(self, state):
        self._settings["force"] = state is True

    def isForced(self):
        return self.retrieveSetting(name="force", default=self._default_settings, namespace="protocol")

    def isReporting(self):
        """Whether the obtained measurements will be reported (i.e. stored/exported)

        :rtype: bool
        """
        return self.retrieveSetting(name="state", default=self.getDefaultSettings(), namespace="exporter") is True

    def doReporting(self, state):
        """Sets whether the measurements obtained by this protocol will be stored/exported

        :type state: bool
        """
        self._settings["exporter.state"] = state is True
        return self


class AbstractParallelProtocol(AbstractCultivatorProtocol):
    """
    A class that is capable to run protocols in parallel to the main stub.
    Most of the work is done by connecting an additional logger to the rootlog which will capture all log messages
    written in the process.
    Implementation should use start instead of execute, start will call prepare which will do some preperations once
    this finished execute is called and at last stop is called to clean things up.
    """

    _default_settings = uvaObject.mergeDictionary(
        AbstractCultivatorProtocol._default_settings,
        {
            "log.file": None,
        })

    _name = "batch_protocol_abstract"

    def __init__(self, settings=None):
        AbstractCultivatorProtocol.__init__(self, settings=settings)
        self._t_start = dt.now()

    def getLogFile(self):
        return self.retrieveSetting(name="file", default=self.getDefaultSettings(), namespace="log")

    def connectLog(self):
        if self.getLogFile() is not None:
            self.connectFileHandler(self.getRootLogger(), self.getLogFile())
            # self.getRootLogger().propagate = False
        self._t_start = dt.now()
        self.getLog().info(" === PROTOCOL START AT: {} ===".format(self._t_start.strftime(self.DT_FORMAT)))

    def connectFileHandler(self, log, log_path=None, log_level=None, propagate=True):
        if log_level is None:
            log_level = self.getLogLevel()
        if log_path is None:
            log_path = self.getLogFile()
        if log_path is not None and os.path.exists(os.path.dirname(log_path)):
            uvaLog.connectFileHandler(log, log_path, level=log_level, propagate=propagate)

    def _prepare(self):
        self.connectLog()
        return super(AbstractParallelProtocol, self)._prepare()

    @abstractmethod
    def _measure(self):
        raise NotImplementedError

    def _clean(self):
        result = super(AbstractParallelProtocol, self)._clean()
        t_end = dt.now()
        self.getLog().info(" === RUN TIME: {:.0f} sec.=== ".format((t_end - self._t_start).total_seconds()))
        self.getLog().info(" === PROTOCOL END AT: {} === ".format(t_end.strftime(self.DT_FORMAT)))
        return result
