# coding=utf-8
# !/usr/bin/env python
"""
Define a measurement protocol, as is a formal definition of what to do when measuring.
"""

import threading, Queue
from uvaCultivator import uvaObject
from uvaCultivatorProtocol import AbstractParallelProtocol as aPP
from uvaSimpleProtocol import SimpleProtocol
from datetime import datetime as dt, timedelta as td
from time import sleep
import numpy as np

__author__ = 'Joeri Jongbloets <j.a.jongbloets@uva.nl>'


class TurbidostatProtocol(SimpleProtocol):
    """A basic turbidostat protocol"""

    _name = "turbidostat"
    _default_settings = SimpleProtocol.mergeDefaultSettings(
        {
            "protocol.use_fake": False,
            "od_led": 720,
            "cultivator_serial": None,
            "turbidostat.collect.time_span": 60,  # retrieve measurements of last hour
            "turbidostat.decision.threshold": 0.35,
            "turbidostat.decision.od_led": 720,
            "turbidostat.decision.breach_metric": "all",  # possible: all, mean, median, mode
            "turbidostat.decision.breach_size": 2,  # number of consecutive breaches need before deciding to dilute
            "turbidostat.pump.mapping": {
                # Pump setting of the form:
                # 0: {"in": "/dev/ttyRP0@1", "out": "/dev/ttyRP1@1"},
            },
            "turbidostat.pump.time": None,  # in seconds, takes precedence over flow - volume
            "turbidostat.pump.rate": 100,  # in RPM
            "turbidostat.pump.flow": 7.7,  # in mL/min
            "turbidostat.pump.volume": 2.4,  # 2.4, # in mL
            "turbidostat.pump.pause": 200,  # wait time in seconds between finishing pump in and pumping out
            "turbidostat.pump.out": True,
            "turbidostat.pump.out.margin": 30, # number of EXTRA seconds to run the pump out
            "reporter.name": "csv",
            #"exporter.file.dir": "",
            #"exporter.file.fmt": path.join("{path}", "{date}_{project_id}_{protocol}_{led}.csv"),
        },
        namespace=""
    )

    def __init__(self, config_file, settings=None):
        super(TurbidostatProtocol, self).__init__(config_file=config_file, settings=settings)
        # stores the collected data
        self._data = {}  # dict of channels with a tuple: ( OD , Decision )
        """ :type: dict[ int, tuple[uvaCultivator.uvaODMeasurement.ODMeasurement, bool]] """
        # importer object
        self._importer = None
        # Scheduler object
        self._scheduler = PumpScheduler(log=self.getLog(), settings=settings)
        # determine pump controller
        from uvaSerial.regloSerialController import regloSerialController as rSC
        if self.fakesConnection():
            from uvaSerial.regloFakeSerialController import regloFakeSerialController as rSC
        self._pump_class = rSC

    def getPumpClass(self):
        return self._pump_class

    def getThreshold(self):
        return self.retrieveSetting(
            name="threshold", default=self.getDefaultSettings(), namespace="turbidostat.decision"
        )

    def getTimeSpan(self):
        return self.retrieveSetting(
            name="time_span", default=self.getDefaultSettings(), namespace="turbidostat.collect"
        )

    def getBreachSize(self):
        return self.retrieveSetting(
            name="breach_size", default=self.getDefaultSettings(), namespace="turbidostat.decision"
        )

    def getBreachMetric(self):
        return self.retrieveSetting(
            name="breach_metric", default=self.getDefaultSettings(), namespace="turbidostat.decision"
        )

    def useWavelengthForDecision(self, led):
        if led in [0, 1]:
            led = [680, 720][led]
        self._settings["turbidostat.decision.od_led"] = led

    def usesWavelengthForDecision(self):
        return self.retrieveSetting(
            name="od_led", default=self.getDefaultSettings(), namespace="turbidostat.decision"
        )

    def getPumpMapping(self):
        """
        Returns the mapping of MC Channels to Pumps
        :return: Dictionary
        :rtype: dict[ int, dict[ str, str]]
        """
        return self.retrieveSetting("mapping", default=self.getDefaultSettings(), namespace="turbidostat.pump")

    def getPumpPauseTime(self):
        return self.retrieveSetting(
            name="pause", default=self.getDefaultSettings(), namespace="turbidostat.pump"
        )

    def getPumpFlow(self):
        result = self.retrieveSetting(
            name="flow", default=self.getDefaultSettings(), namespace="turbidostat.pump"
        )
        if result is None:
            volume = self.getPumpVolume()
            time = self.getPumpTime()
            if None not in (volume, time):
                result = volume / float(time)
        return result

    def getPumpTime(self):
        result = self.retrieveSetting(
            name="time", default=self.getDefaultSettings(), namespace="turbidostat.pump"
        )
        if result is None:
            volume = self.getPumpVolume()
            flow = self.getPumpFlow()
            if None not in (volume, flow):
                result = (volume / float(flow)) * 60.0
        return result

    def getPumpVolume(self):
        result = self.retrieveSetting(
            name="volume", default=self.getDefaultSettings(), namespace="turbidostat.pump"
        )
        if result is None:
            t = self.getPumpTime()
            flow = self.getPumpFlow()
            if None not in (t, flow):
                result = t * float(flow)
        return result

    def runsPumpOut(self):
        return self.retrieveSetting(
            name="out", default=self.getDefaultSettings(), namespace="turbidostat.pump"
        ) is True

    def runPumpOut(self, state):
        self._settings['turbidostat.pump_out'] = state is True
        return self.runsPumpOut()

    def getPumpOutMargin(self):
        return self.retrieveSetting(
            name="margin", default=self.getDefaultSettings(), namespace="turbidostat.pump.out"
        )

    def getPump(self, mapping, channel, direction="in"):
        """

        :type mapping: dict
        :type channel: int
        :type direction: str
        :return:
        :rtype: tuple[ None or str, None or int]
        """
        result = (None, None)
        if channel in mapping:
            if direction in mapping[channel]:
                pump_url = mapping[channel][direction]
                if pump_url is not None and "@" in pump_url:
                    result = pump_url.split("@")
                    result = (result[0], int(result[1]))
        return result

    def addDecision(self, channel, decision, od_value, t_point=None, **attributes):
        """Add a decision and accompanying od value for a given channel to the dataset.

        :type channel: int
        :type decision: bool
        :type od_value: float
        :type t_point: None or datetime.datetime
        :return: The stored decision
        :rtype: dict[str, datetime.datetime or bool or float] or None
        """
        if t_point is None:
            t_point = dt.now()
        data = {}
        data.update(attributes)
        data["t_point"] = t_point
        data["od_value"] = od_value
        data["decision"] = decision
        self._data[channel] = data
        return self.getDecision(channel)

    def getDecision(self, channel):
        """
        Return the decision for the given channel
        :param channel: The index of the channel
        :type channel: int
        :return: Tuple with the decision (True or False) on first position and OD value on second.
        :rtype: tuple[datetime.datetime, bool, float] or None
        """
        result = None
        if channel in self._data.keys():
            result = self._data[channel]
        return result

    def getScheduler(self):
        """
        Return the pumpScheduler object
        :return:
        :rtype: PumpScheduler or None
        """
        return self._scheduler

    def getMeasurementFromDict(self, measurements, channel, key):
        result = None
        if channel in measurements:
            if key in measurements[channel]:
                result = measurements[channel][key]
        if isinstance(result, list):
            self.getLog().debug("Measurement is a list of size {}".format(len(result)))
            result = result[0]
        return result

    def startScheduler(self):
        """Start separate thread that controls the pump"""
        if self._scheduler is None:
            self._scheduler = PumpScheduler(self._settings)
        if not self._scheduler.isAlive():
            self._scheduler.start()
        return self._scheduler.isAlive()

    def stopScheduler(self, graceful=True):
        """Stop separate thread"""
        if self._scheduler is not None:
            self._scheduler.stop(graceful=graceful)
            if graceful:
                self.getLog().info("Waiting for the pump Scheduler to exit")
                while self._scheduler.isAlive():
                    try:
                        # block until scheduler is finished
                        sleep(1)
                    except KeyboardInterrupt:
                        self.getLog().warning("Received keyboard interrupt: force scheduler to stop")
                        break
                if self._scheduler.isAlive():
                    self._scheduler.stop(False)
            self._scheduler = None
        return self._scheduler is None

    def _prepare(self):
        result = super(TurbidostatProtocol, self)._prepare()
        success = self.startScheduler()
        self.getLog().info("Start Scheduler: {}".format(success))
        return result and success

    def _measure(self):
        """Execute the measurement part of the protocol"""
        raise NotImplementedError

    def _react(self):
        """Execute the reaction part of the protocol"""
        raise NotImplementedError

    def _report(self, decisions=None):
        result = False
        if decisions is None:
            decisions = self._data
        if len(decisions) > 0 and self.getExporterName() is not None:
            with self.createExporter() as exporter:
                dest = exporter.getDestination()
                if exporter.hasStorage():
                    result = exporter.addTurbidostatDecisions(decisions)
                    self.getLog().info("Measurements written to {}".format(dest))
                else:
                    self.getLog().error("Could not write measurements to {}".format(dest))
        return result

    def _clean(self):
        result = SimpleProtocol
        # wait until scheduler is finished
        success = self.stopScheduler(graceful=True)
        self.getLog().info("Stop Scheduler: {}".format(success))
        # disconnect pump
        success = PumpScheduler.disconnectActivePumps()
        self.getLog().info("Disconnect active pumps: {}".format(success))
        return result and success

    def generateFakeMeasurement(self, t_point, od):
        from uvaCultivator.uvaODMeasurement import ODMeasurement
        attributes = {
            't_point': t_point,
            'od_value': 1,
            'intensity': 20,
        }
        result = ODMeasurement(**attributes)
        result.setCalibratedODValue(od)
        return result

    def collect(self):
        """
        Collects the measurements from a source
        :return:
        :rtype: dict[ int, dict[ datetime.datetime, list[uvaCultivator.uvaODMeasurement.ODMeasurement]]]
        """
        result = {}
        # load data from CSV or orientDB
        # we use exporter for legacy as it is the data handler, might consider to change the name ;)
        self._importer = self.createExporter()
        self.getLog().info("Read measurements from {}".format(self._importer.getDestination()))
        # retrieve the latest data from a certain period
        t_now = dt.now()
        t_start = t_now - td(minutes=self.getTimeSpan())
        self.getLog().info("Collect measurements from the last {} minutes".format(self.getTimeSpan()))
        # read all measurements from the last hour
        if self.isReporting():
            if self._importer.hasStorage():
                result = self._importer.readODMeasurements(
                    start_time=t_start, criteria={"od_led": self.usesWavelengthForDecision()}
                )
        else:
            result = {}
            for c in [0, 1, 3]:
                result[c] = {}
                t_points = range(0, self.getTimeSpan(), 5)
                for t_min in t_points:
                    t = dt.now() - td(minutes=t_min)
                    # generate a random factor between 0.8 and 1.2
                    f = round(np.random.normal(self.getThreshold(), 0.05))
                    # f = round((np.random.randint(0, 10) * 0.04) + 0.8, 3)
                    result[c][t] = self.generateFakeMeasurement(t, self.getThreshold() * f)
        return result

    def reduce(self, measurements, size=None):
        """Will the number of measurements to the number of measurements considered

        :type measurements: dict[datetime.datetime, list[uvaCultivator.uvaODMeasurement.ODMeasurement]]
        :rtype: list[uvaCultivator.uvaODMeasurement.ODMeasurement]
        """
        results = []
        if size is None:
            size = self.getBreachSize()
        # order time points from high to low
        t_points = sorted(measurements.keys(), reverse=True)
        """:type: list[datetime.datetime]"""
        t_idx = 0
        while len(t_points) > 0 and len(results) < size:
            t_point = t_points.pop(0)
            ms = measurements[t_point]
            if isinstance(ms, (list, tuple)):
                for measurement in ms:
                    results.append(measurement)
                    t_idx += 1
            else:
                results.append(ms)
            del ms
            t_idx += 1
        return results

    def decide(self, measurements):
        raise NotImplementedError

    def dilute(self, channel):
        """
        Dilutes a channel; e.g. schedule adding media and schedule the removal
        """
        # schedule adding media
        t_in = dt.now()
        result = self.schedule(mc_channel=channel, start_time=t_in, persistent=True)
        # schedule media removal
        if result and self.runsPumpOut():
            # set out
            t_out = t_in + td(seconds=self.getPumpPauseTime())
            # get margin in running pump out
            pump_margin = self.getPumpOutMargin()
            # calculate time to run pump out
            pump_time = self.getPumpTime() + pump_margin
            result = self.schedule(mc_channel=channel, start_time=t_out, direction='out', pump_time=pump_time)
        return result

    def schedule(self, mc_channel, start_time, direction="in", persistent=False, pump_time=None):
        """
        Schedule a pump
        """
        result = False
        try:
            if self._scheduler is None:
                self._scheduler = PumpScheduler()
            port, channel = self.getPump(self.getPumpMapping(), channel=mc_channel, direction=direction)
            p_settings = self.reduceNameSpace(self.getSettings(), "turbidostat")
            p_settings['pump.persistent'] = persistent
            if pump_time is not None:
                p_settings["pump.time"] = pump_time
            klass = self.getPumpClass()
            self._scheduler.schedule(klass, port=port, channel=channel, start_time=start_time, settings=p_settings)
            result = True
        except Exception:
            self.getLog().error(
                "\n\t[TurbidostatProtocol] Unable to schedule a {} pump for channel {}.".format(
                    direction, mc_channel), exc_info=1
            )
        return result


class ParallelTurbidostatProtocol(TurbidostatProtocol, aPP):
    """
    This class will do OD Measurements and after that adjust the light. It's capabale
    """

    def __init__(self, config_file, **kwargs):
        aPP.__init__(self, **kwargs)
        TurbidostatProtocol.__init__(self, config_file=config_file, **kwargs)
        self._name = "batch_turbidostat"

    # No need to override the execute function to redirect, becasue it will automatically select the Cultivation execute
    # def execute(self):
    # 	CultivationProtocol.execute(self)


class pumpJob(uvaObject):
    """
    A job to pump in a schedule
    """

    _namespace = "pump.schedule.job"
    _default_settings = uvaObject.mergeDictionary(
        uvaObject._default_settings,
        {
            "persistent": False,
            "channel": None,
            "flow": 0,
            "volume": 0,
            "time": None,
            "rate": 100
        }
    )

    def __init__(self, t_start, pump, settings=None, **kwargs):
        settings = self.mergeDictionary(settings, kwargs, source_namespace=self._namespace)
        uvaObject.__init__(self, settings=settings)
        self._t_start = t_start
        self._pump = pump

    def getTimeStart(self):
        """
        Return the time this job scheduled
        :return:
        :rtype: datetime.datetime
        """
        return self._t_start

    def shouldRun(self):
        return self.getTimeStart() <= dt.now()

    def isConnected(self):
        return self.getPump().isConnected()

    def isPersistent(self):
        return self.retrieveSetting(
            name="persistent", default=self.getDefaultSettings()
        ) is True

    def connect(self):
        try:
            result = self.getPump().connect()
        except Exception as e:
            self.getLog().critical("[PumpJob] Unable to connect:\n>>>\n{}\n<<<".format(e))
            result = False
        return result

    def disconnect(self):
        try:
            result = self.getPump().disconnect()
        except Exception as e:
            self.getLog().critical("[PumpJob] Unable to disconnect:\n>>>\n{}\n<<<".format(e))
            result = False
        return result

    def getPump(self):
        """
        :return:
        :rtype: uvaSerial.regloSerialController.regloSerialController
        """
        return self._pump

    def getPumpPort(self):
        return self.getPump().getPort()

    def getPumpChannel(self):
        return self.retrieveSetting(
            name="channel", default=self.getDefaultSettings()
        )

    def getPumpRate(self):
        return self.retrieveSetting(
            name="rate", default=self.getDefaultSettings()
        )

    def getPumpFlow(self):
        return self.retrieveSetting(
            name="flow", default=self.getDefaultSettings()
        )

    def getPumpVolume(self):
        return self.retrieveSetting(
            name="volume", default=self.getDefaultSettings()
        )

    def getPumpTime(self, flow=None, volume=None):
        result = self.retrieveSetting(name="time", default=self.getDefaultSettings())
        if result is None:
            if flow is None:
                flow = self.getPumpFlow()
            if volume is None:
                volume = self.getPumpVolume()
            if None not in [volume, flow]:
                result = (volume / flow) * 60
        return result

    def run(self):
        try:
            if not self.isConnected():
                self.connect()
            if self.isConnected():
                c = self.getPumpChannel()
                # dummy command so the connection is
                self.getPump().isRunning(c)
                self.getPump().stopPump(c)
                # self.getPump().setPumpMode(c, self.getPump().PUMP_MODE_RPM)
                self.getPump().setPumpMode(c, self.getPump().PUMP_MODE_TIME)
                self.getPump().setSpeedRPM(c, self.getPumpRate(), rate_mode=False)
                # self.getPump().setFlowMode(c, False)  # use RPM
                self.getPump().setRotationDirection(c)
                self.getPump().setDispenseTime(c, self.getPumpTime())
                self.getPump().startPump(c)
                self.getLog().info("[pumpJob] Run pump {}@{} at {}RPM for {:6.2f} sec.".format(
                    c, self.getPump().getPort(), self.getPumpRate(), self.getPumpTime()
                ))
                if not self.isPersistent():
                    self.disconnect()
            else:
                raise Exception("[pumpJob] Unable to connect to pump {}@{}".format(
                    self.getPumpChannel(), self.getPump().getPort()
                ))
        except Exception as e:
            self.getLog().critical("[pumpJob] Exception raised while trying to pump:\n>>>\n{}\n<<<".format(e))


class PumpScheduler(uvaObject, threading.Thread):
    #
    _update = threading.Event()
    # signals we should exit
    _exit = threading.Event()
    # signals we must exit
    _terminate = threading.Event()
    _queue_lock = threading.Lock()
    _queue = Queue.Queue()
    _pump_lock = threading.Lock()
    _active_pumps = {}
    """ :type: dict[str, list[uvaSerial.regloSerialController.regloSerialController, int]]"""

    def __init__(self, log=None, settings=None):
        super(PumpScheduler, self).__init__(settings=settings)
        if log is not None:
            self._log = log
        threading.Thread.__init__(self)
        self._schedule = []
        """ :type: list[pumpJob] """

    def run(self):
        """

        :return:
        :rtype:
        """
        self._terminate.clear()
        self._exit.clear()
        # only stop if we get signalled or there is nothing to do!
        while not self.mustExit() and not (self.shouldExit() and self.canExit()):
            try:
                self._update.clear()
                # check if we have new pumps
                self.processQueue()
                # check if it is time to start some pumps
                self.processSchedule()
                # remove idle pumps
                self.cleanActivePumps()
                # sleep 10 seconds; but be able to wake up if we get signaled
                if not self.hasUpdate():
                    self._update.wait(1)
            except Exception as e:
                self.getLog().critical(
                    "[PumpScheduler] Exception raised during run loop:\n>>>\n%s\n<<<" % e,
                    exc_info=1
                )
        if self.shouldExit() and self.canExit():
            self.getLog().info("Could exit so I did")
        if self.mustExit():
            self.getLog().info("Received terminate signal: exiting")
        # we should stop; so close all remaining pump connections
        self.disconnectActivePumps()

    def hasUpdate(self):
        return self._update.isSet()

    def mustExit(self):
        return self._terminate.isSet()

    def shouldExit(self):
        return self._exit.isSet()

    def canExit(self):
        return not self.hasJobsWaiting() and not self.hasJobsScheduled()

    def hasJobsScheduled(self):
        return len(self._schedule) > 0

    def hasJobsWaiting(self):
        return self._queue.unfinished_tasks > 0

    @classmethod
    def stop(cls, graceful=True):
        if graceful:
            cls._exit.set()
        else:
            cls._terminate.set()
        cls._update.set()

    def processQueue(self):
        """ Reads the queue and puts the job in the schedule

        :return:
        """
        while self._queue.unfinished_tasks:
            job = self.readJob()
            if job is not None:
                self.scheduleJob(job)

    def processSchedule(self):
        """ Reads the schedule and tries to start jobs"""
        new_schedule = []
        for job in self._schedule:
            if job.shouldRun():
                self.runJob(job)
            else:
                new_schedule.append(job)
        self._schedule = new_schedule

    def readJob(self):
        """ Reads a Job from the queue

        :rtype: None or pumpJob
        """
        result = None
        with self._queue_lock:
            try:
                result = self._queue.get(block=False)
                self._queue.task_done()
            except Queue.Empty():
                pass
        return result

    @classmethod
    def storeJob(cls, job):
        result = False
        if job is not None:
            with cls._queue_lock:
                cls._queue.put(job)
                result = True
        return result

    def scheduleJob(self, job):
        self._schedule.append(job)

    def runJob(self, job):
        self.getLog().info(
            "[PumpScheduler] Start Job at {} (Scheduled for {})".format(
            job.getTimeStart().strftime(self.DT_FORMAT),
            dt.now().strftime(self.DT_FORMAT)
        ))
        result = job.run()
        # always deattach
        self.deattachActivePump(job.getPump())
        return result

    @classmethod
    def notify(cls):
        cls._update.set()

    @classmethod
    def hasActivePump(cls, port):
        result = False
        try:
            with cls._pump_lock:
                result = port in cls._active_pumps.keys()
        except Exception as e:
            cls.getLog().critical("[PumpScheduler] Unable to determine if {} is active:\n>>>\n{}\n<<<".format(
                port, e
            ))
        return result

    @classmethod
    def getActivePump(cls, port):
        result = None
        try:
            with cls._pump_lock:
                if port in cls._active_pumps.keys():
                    result = cls._active_pumps[port][0]
        except Exception as e:
            cls.getLog().critical("[PumpScheduler] Unable to get pump object for {}:\n>>>\n{}\n<<<".format(
                port, e
            ))
        return result

    @classmethod
    def getActiveUsers(cls, port):
        result = None
        try:
            with cls._pump_lock:
                if port in cls._active_pumps.keys():
                    result = cls._active_pumps[port][1]
        except Exception as e:
            cls.getLog().critical("[PumpScheduler] Unable to get number of users for {}:\n>>>\n{}\n<<<".format(
                port, e
            ))
        return result

    @classmethod
    def addActivePump(cls, pump):
        """

        :param pump:
        :type pump: uvaSerial.regloSerialController.regloSerialController
        :return:
        :rtype:
        """
        result = None
        try:
            with cls._pump_lock:
                cls._active_pumps[pump.getPort()] = [pump, 1]
            result = pump
        except Exception as e:
            cls.getLog().critical("[PumpScheduler] Unable to add pump object of {}:\n>>>\n{}\n<<<".format(
                pump.getPort(), e
            ))
        return result

    @classmethod
    def addPumpUser(cls, port):
        """

        :return:
        :rtype:
        """
        result = None
        try:
            with cls._pump_lock:
                if port in cls._active_pumps.keys():
                    pump, users = cls._active_pumps[port]
                    cls._active_pumps[port][1] = users + 1
                    result = pump
        except Exception as e:
            cls.getLog().critical("[PumpScheduler] Unable to add pump object of {}:\n>>>\n{}\n<<<".format(
                port, e
            ))
        return result

    @classmethod
    def removeActivePump(cls, pump):
        """

        :param pump:
        :type pump: uvaSerial.regloSerialController.regloSerialController
        :return:
        :rtype:
        """
        result = False
        try:
            with cls._pump_lock:
                result = cls._active_pumps.pop(pump.getPort(), default=None)
            result = result is not None
        except Exception as e:
            cls.getLog().critical("[PumpScheduler] Unable to remove pump object of {}:\n>>>\n{}\n<<<".format(
                pump.getPort(), e
            ))
        return result

    @classmethod
    def deattachActivePump(cls, pump):
        """
        Reduces the number of user of this pump
        :param pump:
        :type pump: uvaSerial.regloSerialController.regloSerialController
        :return: true if succesfully deattached
        :rtype: bool
        """
        result = False
        try:
            with cls._pump_lock:
                if pump.getPort() in cls._active_pumps.keys():
                    pump, users = cls._active_pumps[pump.getPort()]
                    cls._active_pumps[pump.getPort()][1] = users - 1
                    result = True
        except Exception as e:
            cls.getLog().critical("[PumpScheduler] Unable to disconnect pump object of {}:\n>>>\n{}\n<<<".format(
                pump.getPort(), e
            ))
        return result

    @classmethod
    def cleanActivePumps(cls):
        result = False
        try:
            with cls._pump_lock:
                for port in cls._active_pumps.keys():
                    pump, users = cls._active_pumps[port]
                    if users == 0:
                        if pump.isConnected():
                            pump.disconnect()
                        cls.getLog().info("[PumpScheduler] No users for inactive pump at {}, so remove it.".format(
                            pump.getPort()
                        ))
                        cls._active_pumps.pop(port)
                        result = True
        except Exception as e:
            cls.getLog().critical("[PumpScheduler] Unable to clean active pump dictionary:\n>>>\n{}\n<<<".format(
                e
            ))
        return result

    @classmethod
    def disconnectActivePumps(cls):
        result = False
        try:
            with cls._pump_lock:
                for port in cls._active_pumps.keys():
                    pump, users = cls._active_pumps[port]
                    if pump.isConnected():
                        pump.disconnect()
                        cls._active_pumps.pop(port)
            result = True
        except Exception as e:
            cls.getLog().critical("[PumpScheduler] Unable to clean active pump dictionary:\n>>>\n{}\n<<<".format(
                e
            ))
        return result

    @classmethod
    def createPump(cls, port, controller_class, settings=None):
        result = None
        if settings is None:
            settings = {}
        try:
            result = cls.getActivePump(port)
            if result is None:
                # create it
                settings["serial.controller.port"] = port
                if controller_class is None:
                    raise ValueError("[createPump] controller is None")
                pump = controller_class(settings)
                if pump is None:
                    raise ValueError("[createPump] pump is None")
                result = cls.addActivePump(pump)
            else:
                cls.addPumpUser(port)
                cls.getLog().info("[PumpScheduler] Recycle Pump object for {}".format(
                    port
                ))
        except Exception as e:
            cls.getLog().critical("[PumpScheduler] Unable to add pump object for {}:\n>>>\n{}\n<<<".format(
                port, e
            ))
        return result

    @classmethod
    def schedule(cls, controller_class, port, channel, start_time, settings=None, **kwargs):
        """

        :param start_time:
        :type start_time:
        :param channel:
        :type channel:
        :param settings:
        :type settings:
        :param kwargs:
        :type kwargs:
        :return:
        :rtype: bool
        """
        result = False
        try:
            # add kwargs
            settings = pumpJob.mergeDictionary(settings, kwargs)
            if controller_class is None:
                raise ValueError("[schedule] Controller is None")
            pump = cls.createPump(port=port, controller_class=controller_class)
            if pump is None:
                raise ValueError("[schedule] Pump is None")
            p_settings = pumpJob.mergeDictionary({}, cls.isolateNameSpace(settings, "pump"))
            job = pumpJob(t_start=start_time, pump=pump, channel=channel, settings=p_settings)
            if job is None:
                raise ValueError("[schedule] Job is None")
            cls.storeJob(job)
            # notify!
            cls.notify()
        except Exception as e:
            cls.getLog().error("[PumpScheduler] Unable to queue pump:\n>>>\n{}\n<<<".format(e))
        return result
