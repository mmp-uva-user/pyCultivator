# coding=utf-8
"""
Basic module layer that implements a dependency on time
"""

from . import *
from uvaDependency import Dependency
from datetime import datetime as dt

__author__ = 'Joeri Jongbloets <j.a.jongbloets@uva.nl>'


class TimeDependency(Dependency):
    """A Dependency object that is dependent on time"""

    def __init__(self, dependency_id, source, variable, target, state=False, settings=None):
        Dependency.__init__(
            self, dependency_id=dependency_id, source=source, variable=variable, target=target, state=state,
            settings=settings
        )
        self._rhythm = None

    @classmethod
    def loadDefault(cls):
        result = super(TimeDependency, cls).loadDefault()
        result.setVariable("time")
        result.setTarget("state")
        return result

    @classmethod
    def canLoadXML(cls, xml):
        """
        Determines if this class is able to load this xml Element
        :type xml: xml.etree.ElementTree.Element
        :rtype: bool
        """
        return super(TimeDependency, cls).canLoadXML(xml) and xml.find("./{uvaCultivator}rhythm") is not None

    @classmethod
    def createFromXMLElement(cls, xml, default=None):
        """
        :param xml:
        :type xml: xml.etree.ElementTree.Element
        :return:
        :rtype: TimeDependency
        """
        result = super(TimeDependency, cls).createFromXMLElement(xml, default=default)
        # parse the rhythm
        rhythm_e = xml.find("./{uvaCultivator}rhythm")
        if isinstance(result, TimeDependency) and rhythm_e is not None:
            rhythm = Rhythm.createFromXMLElement(rhythm_e)
            result.setRhythm(rhythm)
        return result

    def setRhythm(self, rhythm):
        self._rhythm = rhythm

    def getRhythm(self):
        """
        :rtype: Rhythm
        """
        return self._rhythm

    def hasRhythm(self):
        return self._rhythm is not None

    def calculate(self, source_value=None):
        """
        :param source_value:
        :type source_value: datetime.datetime
        :return:
        :rtype: bool
        """
        result = None
        if source_value is None or not isinstance(source_value, dt):
            source_value = dt.now()
        self.getLog().debug("The time dependency is {}active".format("" if self.isActive() else "not "))
        if self.isActive() and self.hasRhythm():
            result = self.getRhythm().calculate(source_value)
        return result


class Rhythm(uvaObject):
    """ An object that describes a time rhythm

    <rhythm start="2015-01-31T09:30:00">
        <period id="0" length="3600">true</period>
        <period id="1" length="50400">false</period>
    </rhythm>
    """

    def __init__(self, start=None, stop=None, periods=None, settings=None):
        uvaObject.__init__(self, settings=settings)
        self._start = start
        if self._start is None:
            self._start = dt.now()
        self._stop = stop
        self._periods = periods
        if self._periods is None:
            self._periods = {0: (1, True)}
        if self.getCycleLength() == 0:
            self.getLog().warning("I don't allow rhythm with no length; so this is now continuous light")
            self._periods[0] = (1, True)

    @staticmethod
    def loadDefault():
        return Rhythm()

    @staticmethod
    def canLoadXML(xml):
        """
        Determines if this class is able to load this xml Element
        :type xml: xml.etree.ElementTree.Element
        :rtype: bool
        """
        return xml.tag == "{uvaCultivator}rhythm"

    @classmethod
    def createFromXMLElement(cls, xml, default=None):
        """
        :param xml: Rhythm Element
        :type xml: xml.etree.ElementTree.Element
        :type default: Rhythm
        :return:
        :rtype: TimeDependency
        """
        from uvaCultivatorConfig.xmlCultivatorConfig import xmlCultivatorConfig as xCC
        result = None
        if default is None:
            default = cls.loadDefault()
        if cls.canLoadXML(xml):
            start_date_time = xCC.validateValue(xml.get("start"), dt, default.getStartDateTime())
            stop_date_tme = xCC.validateValue(xml.get("stop"), dt, default.getStopDateTime())
            periods_e = xml.findall("./{uvaCultivator}period")
            periods = {}
            for period_e in periods_e:
                id = xCC.validateValue(period_e.get("id"), int, 0)
                length = xCC.validateValue(period_e.get("length"), int, 0)
                state = xCC.validateValue(period_e.text, bool, False)
                periods[id] = (length, state)
            result = Rhythm(start=start_date_time, stop=stop_date_tme, periods=periods)
        return result

    def getPeriod(self, idx):
        result = None
        if idx <= len(self._periods):
            result = self._periods[idx]
        return result

    def getPeriods(self):
        """
        :rtype: dict
        """
        return self._periods

    def getStartDateTime(self):
        """
        :rtype: datetime.datetime
        """
        return self._start

    def getStopDateTime(self):
        """
        :rtype: datetime.datetime
        """
        return self._stop

    def getCycleLength(self):
        result = 0
        for period in self.getPeriods().keys():
            # period = ( duration , state )
            result += self.getPeriod(period)[0]
        return result

    def isActive(self, t_now):
        return self._start <= t_now and (self._stop is None or t_now <= self._stop)

    def calculate(self, t_now=None):
        """
        Will return the desired light state as dictated by this regime
        :return:
        :rtype: None, bool
        """
        if t_now is None:
            t_now = dt.now()
        dState = None
        # testSerial if we are active
        if self.isActive(t_now):
            # First we need to calculate where we are in a cycle ON_PERIOD + OFF_PERIOD = 1 cycle
            # calculate remainder as int -> number of seconds in cycle
            distance = int((t_now - self.getStartDateTime()).total_seconds())
            cycle_length = self.getCycleLength()
            cycle_time = distance % cycle_length
            # OK the first ON_PERIOD seconds -> the light is ON
            idx = 0
            keys = sorted(self._periods.keys())
            while cycle_time >= 0 and idx < len(keys):
                cycle_time -= self._periods[keys[idx]][0]
                if cycle_time <= 0:
                    dState = self._periods[keys[idx]][1]
                idx += 1
            # elif cycle_time > self._on_period: dState = False
            self.getLog().debug(
                """We have a cycle of {0} seconds. After {1} cycles we are now {2} seconds in a cycle.
                This means that the desired light state is {3}.""".format(
                    self.getCycleLength(),
                    int((t_now - self.getStartDateTime()).total_seconds() / self.getCycleLength()),
                    int((t_now - self.getStartDateTime()).total_seconds() % self.getCycleLength()),
                    "On" if dState else "Off"
                ))
        # end if
        return dState
