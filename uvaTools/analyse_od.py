#!/usr/bin/env python
# coding=utf-8
"""
Analyse the OD Error distribution.
"""

__author__ = 'Joeri Jongbloets <j.a.jongbloets@student.vu.nl>'

import logging, sys, traceback
from datetime import datetime as dt
from os import path as OP
cwd = OP.dirname(OP.realpath(__file__))
sys.path.insert(1, OP.realpath("{}/../".format(cwd)))
from time import sleep
from uvaSerial.psiSerialController import psiSerialController as pSC2
#from uvaSerial.psiFakeSerialController import psiFakeSerialController as pFSC
from uvaSerial import uvaSerialLogger as uSL

LOG_LEVEL = logging.INFO        # minimum level of messages to log
CHANNELS = [0, 2, 4, 6] #[1] #[1,3,5,7]  #[0, 1, 2, 3, 4, 5, 6, 7]
# Number of measurements made by the MC and averaged before sending to the computer
OD_REPEATS = 1
# Store OD LED
OD_LEDS = [0, 1] # 0 == 680 1 == 720
# Set whether it should stop the gas
GAS_STOP = True
# Set time to wait after turning gas pump off
GAS_PAUSE = 0
# Set whether it should try to restore the light
LIGHT_RESTORE = False
# Number of (sets of) measurements to be performed by the computer
OD_MEASUREMENTS = 1000

if __name__ == "__main__":
	t_start = dt.now()
	settings = {
		#"port" : "/dev/ttyMC2"
		"port": "/dev/tty.usbserial-FTAK08XU",# mc 0
		#"port" : "/dev/tty.usbserial-FTGZPIMR", # mc 2
		# "port": "/dev/tty.usbserial-FTGZOFXY", # mc 1
		"stopBits" : 1,
		"parity" : "N",
		"byteSize" : 8
	}
	fp = "../data/{}_analyse_od.csv".format(t_start.strftime('%Y%m%d'))
	logfile = "../log/{}_analyse_od.log".format(t_start.strftime('%Y%m%d'))
	# if len(sys.argv) >= 2:
	# 	device = sys.argv[1]
	# if len(sys.argv) >= 3:
	# 	fp = sys.argv[2]        # use a different report file
	# if len(sys.argv) >= 4:
	# 	logfile = sys.argv[3]
	# if len(sys.argv) >= 5:
	# 	OD_MEASUREMENTS = int(sys.argv[4])
	logger = uSL.addUvaLogFileHandler(uSL.connectLogger(), logfile, LOG_LEVEL)
	logger.setLevel(LOG_LEVEL)
	uSL.addUvaLogStreamHandler(logger, LOG_LEVEL)
	print "Connected FileHandler to path {} and logger {}".format(logfile, logger.name)
	conn = None
	data = {}  # dt : { attributes }
	idx = 0
	total_measurements = len(CHANNELS) * len(OD_LEDS) * OD_MEASUREMENTS
	try:
		conn = pSC2(settings=settings)
		conn.configure(**settings)
		""" :type: uvaSerial.psiSerialController.psiSerialController |
		uvaSerial.psiFakeSerialController.psiFakeSerialController """
		#conn = pFSC(port=device)
		if conn.connect():
			logger.info(" === LOG START AT: {} ===".format(t_start.strftime('%Y-%m-%d %H:%M:%S')))
			logger.info("Connected to {}".format( conn.getPort() ))
			logger.info("Will write measurements to {}".format(fp))
			result = False
			if not GAS_STOP or conn.setPumpState():
				logger.info("Pump has been switched off, wait {} seconds".format(GAS_PAUSE))
				if GAS_STOP:
					sleep(GAS_PAUSE)  # wait until bubbles are gone
				# switch lights off
				if conn.setAllLightStates():
					logger.info("Light has been switched off, start measurements")
					# ok light is off
					for i in range(OD_MEASUREMENTS):
						for c in CHANNELS:
							for od_led in OD_LEDS:
								# Increment current index
								t_now = dt.now()
								reading = conn.getODReading(channel=c, led=od_led, repeats=OD_REPEATS)
								sample = {
									"LED" : 680 if od_led == 0 else 720,
									"channel" : c,
									"OD" : reading[2]
								}
								if t_now in data:
									data[t_now].append(sample)
								else:
									data[t_now] = [sample]
								# Show every 10 percent progress
								if idx % (total_measurements * 0.1) == 0:
									logger.info("Measured {}% (idx = {})".format(
										idx / (total_measurements * 0.01),
										idx
									))
								idx += 1
						# all repeats done
					# all channels done
					logger.info("Completed all OD measurements.")
			else:
				conn.getLog().critical("Unable to switch gas pump OFF")
			# turn gas back on
			# if gas_state and not conn.getSerial().setPumpState(True):
			# 	conn.getLog().critical("Unable to switch gas pump ON")
			# restore light
			if LIGHT_RESTORE and not conn.setAllLightStates(state=True):
				conn.getLog().critical("Unable to restore light")
			if GAS_STOP and not conn.setPumpState(state=True):
				conn.getLog().critical("Unable to restore gas")
			# now process measurements
			if len(data.keys()) > 0:
				with open(fp, "w") as dest:
					headers = ["channel", "LED", "OD"]
					#dest.write("{};{}\n".format(t_start.strftime("%Y-%m-%d %H:%M:%S"), ";".join(headers)))
					for key in sorted(data.keys()):
						for idx in range(len(data[key])):
							variables = []
							line = key.strftime("%S.%f")
							for header in headers:
								if header in data[key][idx].keys():
									line = "{};{}".format(line, data[key][idx][header] )
							dest.write("{}\n".format(line))
						# end of for
					# end of for
				# end of with
			# end of if
		else:
			conn.getLog().critical("Unable to configure or to connect to {}".format(settings["port"]))
	except Exception as e:
		msg = "Error:\n{}".format(traceback.format_exc())
		if logger:
			logger.critical(msg)
		else:
			print msg
	finally:
		if conn is not None and conn.isConnected():
			t_end = dt.now()
			conn.getLog().info(" === RUN TIME: {} sec.===".format((t_end-t_start).seconds))
			conn.getLog().info(" === LOG END AT: {} ===".format(t_end.strftime('%Y-%m-%d %H:%M:%S')))
			conn.disconnect()
