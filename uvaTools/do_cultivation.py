#!/usr/bin/env python
# coding=utf-8
"""
A script executing a protocol that measures OD and subsequently adjusts the light settings based on the a regime.
"""

__author__ = 'Joeri Jongbloets <j.a.jongbloets@uva.nl>'

import sys, os, argparse, logging
cwd = os.path.dirname(__file__)
sys.path.insert(1, os.path.realpath(os.path.join(cwd, "..")))
# get all necessary imports
from do_measure_od import simpleScript, MeasureODProtocol

# Set minimum level of messages that will be written to the log
LOG_LEVEL = logging.INFO


class CultivationProtocol(MeasureODProtocol):
    """A cultivation protocol, largely based on the MeasureODProtocol and extended to also adjust the light"""

    _name = "cultivation"

    _default_settings = MeasureODProtocol.mergeDefaultSettings(
        {
            "protocol.od_led": 720,
            "protocol.force_state": True,
            "protocol.force_intensity": True,
            "protocol.equal_error": 1  # in uE
        }, namespace=""
    )

    def __init__(self, config_file, settings=None):
        MeasureODProtocol.__init__(self, config_file=config_file, settings=settings)
        self._light_states = []

    def getWavelength(self):
        """Returns the wavelength used to set the light intensity"""
        result = self.retrieveSetting("od_led", namespace="protocol")
        return 680 if result in (0, 680) else 720

    def isStateForced(self):
        """Returns whether the protocol is forced to set the state to configured value instead of current value"""
        return self.retrieveSetting("force_state", namespace="protocol") is True

    def isIntensityForced(self):
        """Returns whether the protocol is forced to set the intensity to the configured value instead of current"""
        return self.retrieveSetting("force_intensity", namespace="protocol") is True

    def getEqualityError(self):
        result = self.getDefaultSettings("equal_error", "protocol")
        error = self.retrieveSetting("equal_error", namespace="protocol")
        try:
            result = float(error)
        except ValueError:
            pass
        return result

    def isEqual(self, lhs, rhs, error=None):
        """Returns whether the lhs is equal to the rhs given a certain error"""
        if error is None:
            error = self.getEqualityError()
        return abs(lhs - rhs) < error

    def adjustLightState(self, channel, start_state=None, write=False):
        """Adjusts the light state using the regime defined in the configuration"""
        # determine the current state of the light
        current_state = self.getCultivator().getChannelLight(channel).getLightState()
        # set the old state of the light (before we started the protocol)
        if start_state is None:
            start_state = current_state
        # usually we will restore the light to the old state
        default_state = start_state
        # but if we are forced we reset the light to configuration values
        if self.isStateForced():
            default_state = self.getCultivator().getChannelLight(channel).getConfiguredLightState()
        # new state = state based on regime or default state
        new_state = self.getCultivator().getChannelLight(channel).getDesiredLightState(default=default_state)
        # only change light states when the new state is different.
        if new_state != current_state:
            # report on change
            self.getLog().info(
                "Current state ({}) doesn't match desired state ({}), adjust it!".format(
                    "On" if current_state else "Off", "On" if new_state else "Off"
                )
            )
            # if we fail to adjust it, report it
            if not self.getCultivator().getChannelLight(channel).setLightState(new_state, simulate=not write):
                self.getLog().warning("FAILED to apply new state to channel {}".format(channel))
                # for administration, keep current state
                new_state = current_state
        return new_state

    def adjustLightIntensity(self, channel, start_intensity=None, write=False):
        """Adjusts the light intensity using the regime defined in the configuration"""
        # determine current intensity
        current_intensity = self.getCultivator().getChannelLight(channel).getLightIntensity()
        # set old intensity (before we started the protocol)
        if start_intensity is None:
            start_intensity = current_intensity
        # default intensity, the intensity that will be used if calculation fails, usually old intensity
        default_intensity = start_intensity
        # but we may be forced to apply the configured intensity
        if self.isIntensityForced():
            default_intensity = self.getCultivator().getChannelLight(channel).getConfiguredLightIntensity()
        # set default od value
        current_od = 0
        # load wavelength that will be used to calculate the regime
        wavelength = self.getWavelength()
        # try to load last OD Measurement
        if self.getCultivator().getChannel(channel).getMeasurement(nm=wavelength, idx=-1) is not None:
            m = self.getCultivator().getChannel(channel).getMeasurement(nm=wavelength, idx=-1)
            current_od = m.getCalibratedODValue()
        # calculate desired intensity
        new_intensity = self.getCultivator().getChannelLight(channel).getDesiredLightIntensity(
            od=current_od, default=default_intensity
        )
        # Test whether the current intensity is in 1 uE/m2/s of the desired intensity
        if not self.isEqual(new_intensity, current_intensity):
            self.getLog().info(
                "Current intensity ({}) doesn't match desired intensity ({}), adjust it!".format(
                    current_intensity, new_intensity
                )
            )
            if not self.getCultivator().getChannelLight(channel).setLightIntensity(new_intensity, simulate=not write):
                self.getLog().warning("FAILED to apply new intensity to channel {}".format(channel))
                new_intensity = current_intensity
        return new_intensity

    def adjustLights(self, channels=None, write=False):
        """Adjusts the light settings to the desired light settings according to the regime defined in the
        configuration

        :rtype: bool
        """
        result = False
        if channels is None:
            channels = self.getCultivator().getChannelRange()
        for channel in channels:
            start_state = None
            if len(self._light_states) > channel:
                start_state = self._light_states[channel]
            future_state = self.adjustLightState(channel=channel, start_state=start_state, write=write)
            # only adjust if future state is on (True)
            if future_state:
                self.adjustLightIntensity(channel=channel, write=write)
        if not write:
            for channel in channels:
                if self.getCultivator().getChannel(channel).hasLight():
                    self.getCultivator().getChannelLight(channel).applyLightSettings()
        result = True
        return result

    def _prepare(self):
        """Prepare for protocol, record current light states"""
        result = MeasureODProtocol._prepare(self)
        if result:
            # create a list of
            self._light_states = [
                self.getCultivator().getChannelLight(c).getLightState() for c in self.getCultivator().getChannelRange()
            ]
        return result

    def _measure(self):
        """The actual protocol"""
        # execute the normal protocol to get the OD measurements
        result = MeasureODProtocol._measure(self)
        # if successful also adjust light
        result = result and self.adjustLights(write=False)
        return result

if __name__ == "__main__":
    protocol = CultivationProtocol
    cs = simpleScript.SimpleScript(__file__, protocol)

    # Setup argument parsing
    parser = argparse.ArgumentParser(description='Run a cultivation using one experiment configuration.')
    # Setup argument parsing
    parser = argparse.ArgumentParser(description='Measure OD using one experiment configuration.')
    parser.add_argument('config_path', action="store",
                        help='Path to the configuration file or the file name (with or without .xml).')
    parser.add_argument('--config-dir', action="store", dest="config_dir",
                        help="Path to the configuration files.")
    # ini settings
    parser.add_argument('--ini', action="store", dest="ini_path",
                        help='Path to the ini file that should be used.')
    # path settings
    parser.add_argument('--output-dir', action="store", dest='output_dir',
                        help='Path to the directory where data file(s) will be stored.')
    parser.add_argument('--log-dir', action="store", dest="log_dir",
                        help='Path to the directory where log file(s) will be stored.')
    # protocol settings
    parser.add_argument('-f', action="store_true", dest='force_run',
                        help='Force running the (inactive) configuration.')
    parser.add_argument('-n', action="store_true", dest='use_fake',
                        help='Simulate a Multi-Cultivator connection instead of using a real connection.')
    parser.add_argument('-s', action="store_true", dest='simulate',
                        help='Prevent script from exporting data.')
    parser.add_argument('-S', action="store_true", dest='simulate_fake',
                        help='Simulate connection and do not export data (equal to -sn).')
    # script settings
    parser.add_argument('-v', action="count", dest='verbosity',
                        help='Enable logging, use multiple flag (i.e. -v -v or -vv) to increase detail')
    # set defaults
    parser.set_defaults(
        # config_dir=cs.getConfigDir(), log_dir=cs.getLogDir(), output_dir=cs.getOutputDir(),
        force_run=cs.isForced(), verbosity=0, simulate_fake=False, ini_path=None,
        simulate=not cs.isReporting(), use_fake=cs.fakesConnection
    )
    # parse the arguments
    args = parser.parse_args()
    # load arguments into the object, we enforce our protocol (so it is not set by the ini file)
    if not cs.loadArguments(args, settings={"protocol.class": protocol}, namespace=""):
        raise Exception("Unable to load arguments, aborting..")
    cs.start()
