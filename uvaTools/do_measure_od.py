#!/usr/bin/env python
# coding=utf-8
"""
Application to measure the OD of a set of channels and subsequently makes adjustments to the light settings based on OD.
"""

__author__ = 'Joeri Jongbloets <j.a.jongbloets@uva.nl>'

import sys, os, argparse
cwd = os.path.dirname(__file__)
sys.path.insert(1, os.path.realpath(os.path.join(cwd, "..")))
# get all necessary imports
from uvaScript import simpleScript
from uvaCultivator.uvaCultivatorProtocol.uvaCultivationProtocol import SimpleProtocol
from time import sleep


class MeasureODProtocol(SimpleProtocol):
    """Class that measures the OD and nothing else."""

    _name = "measure_od"

    def _prepare(self):
        """Prepare for the protocol: in this case make sure we are connected"""
        # first do normal preparation stuff
        result = super(MeasureODProtocol, self)._prepare()
        # only try to connect if config is active or when forced
        connected = self.getConnection().isConnected()
        if result and not connected:
            connected = self.getConnection().connect()
            if not connected:
                self.getLog().critical("Unable to configure or to connect to {}".format(
                    self.getConnection().getSerial().getPort()
                ))
        # return success: we should be connected now
        self.getLog().info("Connect to Multi-Cultivator: {}".format(result))
        return connected

    def _measure(self):
        """The actual protocol"""
        result = False
        current_states = {}
        try:
            # turn the gas flow off
            state = self.getConnection().setPumpState()
            if not state:
                raise Exception("Unable to turn gas pump OFF")
            gas_pause = self.getCultivator().getGasPause()
            self.getLog().info("Have gas pause of {} seconds".format(gas_pause))
            # wait until all (or most) bubbles left the culture
            if gas_pause > 0:
                sleep(gas_pause)
            # turn the light off
            current_states = self.getCultivator().setLightsState()
            if len(current_states) == 0:
                raise Exception("Unable to turn lights OFF")
            # wait for one second before measuring the OD
            sleep(1)
            self._measurements = self.getCultivator().measureOD()
            result = True
        except KeyboardInterrupt:
            self.getLog().critical("[SimpleProtocol] Interrupted by user, quit gracefully")
        except Exception as e:
            self.getLog().critical("[SimpleProtocol] Unexpected Error:\n{}".format(e))
        finally:
            if not self.getConnection().isConnected():
                self.getLog().info("Controller was disconnected, reconnect")
                self.getConnection().connect()
            # ok restore light state and gas pump
            # it doesn't matter if it fails, we tried it!
            self.getCultivator().getSerial().setPumpState(True)
            for c in sorted(current_states.keys()):
                # write log message about light settings
                self.getLog().info(
                    "Light @ channel {}, is {}.".format(c, "switched back on" if current_states[c] else "left off")
                )
                if self.getCultivator().getChannel(c).hasLight() and current_states[c]:
                    self.getCultivator().getChannel(c).setLightState(current_states[c])
        # end of finally
        return result

    def _clean(self):
        # check if we are connected
        connected = self.hasConnection() and self.getConnection().isConnected()
        # set connection state by either being not connected or by successful disconnecting
        connected = not connected or not self.getConnection().disconnect()
        self.getLog().info("Disconnect from Multi-Cultivator: {}".format(not connected))
        # do normal clean stuff
        result = super(MeasureODProtocol, self)._clean()
        return result and not connected

if __name__ == "__main__":
    protocol = MeasureODProtocol
    cs = simpleScript.SimpleScript(__file__, protocol)

    # Setup argument parsing
    parser = argparse.ArgumentParser(description='Measure OD using one experiment configuration.')
    parser.add_argument('config_path', action="store",
                        help='Path to the configuration file or the file name (with or without .xml).')
    parser.add_argument('--config-dir', action="store", dest="config_dir",
                        help="Path to the configuration files.")
    # ini settings
    parser.add_argument('--ini', action="store", dest="ini_path",
                        help='Path to the ini file that should be used.')
    # path settings
    parser.add_argument('--output-dir', action="store", dest='output_dir',
                        help='Path to the directory where data file(s) will be stored.')
    parser.add_argument('--log-dir', action="store", dest="log_dir",
                        help='Path to the directory where log file(s) will be stored.')
    # protocol settings
    parser.add_argument('-f', action="store_true", dest='force_run',
                        help='Force running the (inactive) configuration.')
    parser.add_argument('-n', action="store_true", dest='use_fake',
                        help='Simulate a Multi-Cultivator connection instead of using a real connection.')
    parser.add_argument('-s', action="store_true", dest='simulate',
                        help='Prevent script from exporting data.')
    parser.add_argument('-S', action="store_true", dest='simulate_fake',
                        help='Simulate connection and do not export data (equal to -sn).')
    # script settings
    parser.add_argument('-v', action="count", dest='verbosity',
                        help='Enable logging, use multiple flag (i.e. -v -v or -vv) to increase detail')
    # set defaults
    parser.set_defaults(
        # config_dir=cs.getConfigDir(), log_dir=cs.getLogDir(), output_dir=cs.getOutputDir(),
        force_run=cs.isForced(), verbosity=0, simulate_fake=False, ini_path=None,
        simulate=not cs.isReporting(), use_fake=cs.fakesConnection
    )
    # parse the arguments
    args = parser.parse_args()
    # load arguments into the
    if not cs.loadArguments(args, settings={"protocol.class": protocol}, namespace=""):
        raise Exception("Unable to load arguments, aborting..")
    cs.start()
