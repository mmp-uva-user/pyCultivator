# coding=utf-8
# !/usr/bin/env python

import sys, traceback, os, logging, argparse

# add current directory to system path (avoid package problems)
cwd = os.path.dirname(os.path.realpath(__file__))
# add path to uvaCultivator
sys.path.insert(1, os.path.join(cwd, "..", "..", ))
# add path to calibrator
sys.path.insert(1, os.path.join(cwd, "..", ))

from PyQt4 import QtGui
from controllers.main import MainController

__author__ = 'Joeri Jongbloets <j.a.jongbloets@uva.nl>'


def start(path):
    result = False
    # set logger
    log = logging.getLogger()
    handler = logging.StreamHandler()
    handler.setLevel(logging.WARNING)
    log.addHandler(handler)
    log.propagate = False
    # load application
    app = QtGui.QApplication(sys.argv)
    try:
        # Load Main Controller
        controller = MainController(app=app)
        controller.setRootPath(path)
        # Start Main Controller
        controller.start(controller=controller, app=app, execute=True)
    except:
        msg = "Error:\n{}".format(traceback.format_exc())
        log.critical(msg)
    finally:
        app.exit(0)


if __name__ == "__main__":
    # create argument parser
    parser = argparse.ArgumentParser(description='Run the calibrator application')
    parser.add_argument('--path', dest="path", action="store",
                        help='Path to the application files (configuration, data, log)')
    # set the default root path
    parser.set_defaults(path=os.path.join(cwd, "..", "..", "conf"))
    # parse arguments
    args = parser.parse_args()
    # Start application
    start(args.path)
