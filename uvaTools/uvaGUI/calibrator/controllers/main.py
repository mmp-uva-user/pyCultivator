# coding=utf-8

import os
from .base import *
from .serial import SerialController, SerialWorker
from .learn import LearnController
from .calibration import CalibrationController
from ..views.main import MainView
from ..models.serial import SerialConnectionModel
from ..models.measurement import ODMeasurementTableModel
from ..models.calibration import CalibrationTableModel
from lxml import etree
from uvaCultivator.uvaCultivatorConfig.xmlCultivatorConfig import xmlCultivatorConfig as xCC

__author__ = 'Joeri Jongbloets <j.a.jongbloets@uva.nl>'


class MainController(BaseController):
    """ Controller for the main window

    """

    saved_changed = QtCore.pyqtSignal(object)
    config_changed = QtCore.pyqtSignal(object)

    def __init__(self, channels=None, wavelengths=None, parent=None, app=None, **kwargs):
        BaseController.__init__(self, parent=parent, app=app, **kwargs)
        # settigns
        self._channels = channels
        if self._channels is None:
            self._channels = range(8)
        self._wavelengths = wavelengths
        if self._wavelengths is None:
            self._wavelengths = [680, 720]
        # configuration
        self._config = None
        self._saved_xml = None
        self._xml_path = None
        self._root_path = None
        # models
        self._serial_model = None
        self._mc_model = None
        self._has_mc_data = False
        self._ind_model = None
        self._has_ind_data = False
        self._pred_model = None
        self._has_pred_data = False
        # view
        self._view = None
        # connection worker
        self._serial_worker = None
        # reset everything
        self.resetSerialModel()
        self.resetSerialWorker()
        self.resetView()
        self.resetCultivatorModel()
        self.resetIndependentModel()
        self.resetPredictionModel()

    @classmethod
    def start(cls, controller=None, parent=None, app=None, execute=False, **kwargs):
        """Starts the controller by loading it"""
        result = False
        try:
            if controller is None:
                controller = cls(parent=parent, app=app, **kwargs)
            if controller.hasView():
                controller.getView().show()
                controller.getView().setFocus()
            if app is not None and execute:
                app.exec_()
            result = True
            controller.stop()
        except:
            raise
        return result

    def stop(self):
        """The stop method of this controller"""
        result = self._saved
        if self.isConnected():
            self.getLog().warning("Serial connection is open, closing it before exiting.")
            self.disconnectSerial()
        if not self.isSaved():
            # open dialog -> whether the user wants to save
            result = self.askToSave()
        if result:
            # remove running threads
            result = self.stopSerialWorker()
            result = BaseController.stop(self) and result
        return result

    def clearTableModel(self, model=None):
        """Clears the table model

        :type model: calibrator.models.measurement.ODMeasurementTableModel
        """
        if model is not None:
            model.clear()

    def resetCultivatorModel(self):
        """Resets and connect the cultivator measurement model object"""
        self._mc_model = ODMeasurementTableModel(channels=self._channels, wavelengths=self._wavelengths, parent=self)
        self.getCultivatorModel().setEditable(False)
        self.connectCultivatorModel()

    def connectCultivatorModel(self):
        """Connects the MC model to the controller and view"""
        self.getView().getUi().tvMC.setModel(self.getCultivatorModel())
        self.getCultivatorModel().dataChanged.connect(self.updateCultivatorModel)
        self.getCultivatorModel().layoutChanged.connect(self.updateCultivatorModel)

    @QtCore.pyqtSlot()
    def updateCultivatorModel(self):
        """Process updates from the cultivator model"""
        self._has_mc_data = self.hasCultivatorModel() and self.getCultivatorModel().hasData()
        self.getView().updateView()
        if self.hasPredictionModel():
            self.getPredictionModel().layoutChanged.emit()

    def resetIndependentModel(self):
        """Resets and connects the independent measurement model object"""
        self._ind_model = ODMeasurementTableModel(channels=self._channels, wavelengths=self._wavelengths, parent=self)
        self.getIndependentModel().setEditable(True)
        self.connectIndependentModel()

    @QtCore.pyqtSlot()
    def updateIndependentModel(self):
        self._has_ind_data = self.hasIndependentModel() and self.getIndependentModel().hasData()
        self.getView().updateView()

    def connectIndependentModel(self):
        """Connects the independent model to the controller and view"""
        self.getView().getUi().tvInd.setModel(self.getIndependentModel())
        self.getIndependentModel().dataChanged.connect(self.updateIndependentModel)
        self.getIndependentModel().layoutChanged.connect(self.updateIndependentModel)

    def resetPredictionModel(self):
        """Resets and connects the calibration table model"""
        self._pred_model = CalibrationTableModel(channels=self._channels, wavelengths=self._wavelengths, parent=self)
        self.connectPredictionModel()

    @QtCore.pyqtSlot()
    def updatePredictionModel(self):
        self._has_pred_data = self.hasPredictionModel() and self.getPredictionModel().hasData()
        self.getView().updateView()

    def connectPredictionModel(self):
        """Connects the prediction model to the controller and view"""
        self.getView().getUi().tvPred.setModel(self.getPredictionModel())
        self.getPredictionModel().dataChanged.connect(self.updatePredictionModel)
        self.getPredictionModel().layoutChanged.connect(self.updatePredictionModel)

    def resetSerialModel(self):
        """Resets and connects the serial model object"""
        self._serial_model = SerialConnectionModel()
        if self.hasConfig():
            self.getSerialModel().load(self.getConfig())
        self.connectSerialModel()
        self.updateSerialModel(self.getSerialModel())

    def connectSerialModel(self):
        """Connects the controller to the model"""
        self.getSerialModel().changed.connect(self.updateSerialModel)

    def resetSerialWorker(self):
        """Resets and connects the serial worker"""
        if self.hasSerialWorker() and self.getSerialWorker().isRunning():
            self.stopSerialWorker()
        # create new worker
        self._serial_worker = SerialWorker(self.getSerialModel())
        self.connectSerialWorker()
        self.getSerialWorker().start()

    def connectSerialWorker(self):
        """Connects the worker to the controller"""
        self.getSerialWorker().connection_changed.connect(self.updateSerialConnection)

    def disconnectSerialWorker(self):
        self.getSerialWorker().connection_changed.disconnect(self.updateSerialConnection)

    def resetView(self):
        """Resets and connect a view to the controller"""
        # close and remove old view
        if self.hasView():
            self.getView().close()
            self._view = None
        # create new view
        self._view = MainView(self, parent=self.getParent())
        self.connectView()

    def connectView(self):
        """Connects the controller to the view"""
        self.config_changed.connect(self.updateConfig)
        self.getView().getUi().cbWavelength.currentIndexChanged.connect(self.updateWavelength)
        self.getView().getUi().pbMeasure.clicked.connect(self.measureSerial)
        self.getView().getUi().pbCalibrate.clicked.connect(lambda: self.calibrateModels())
        self.getView().getUi().pbAddRow.clicked.connect(lambda: self.getIndependentModel().addRow())
        self.getView().getUi().pbClearMC.clicked.connect(
            lambda: self.clearTableModel(self.getCultivatorModel())
        )
        self.getView().getUi().pbClearInd.clicked.connect(
            lambda: self.clearTableModel(self.getIndependentModel())
        )
        self.getView().getUi().pbPredReset.clicked.connect(
            lambda: self.resetCalibrationModels(self.getPredictionModel().getActiveWavelength())
        )

    @QtCore.pyqtSlot(object)
    def updateSerialModel(self, model):
        """Process update from the serial model"""

        self._saved = False
        # reset worker to reload model
        if self.hasSerialWorker():
            self.getSerialWorker().setModel(model)
            if self.getSerialWorker().isConnected():
                self.getSerialWorker().do_reset()

    @QtCore.pyqtSlot(object, bool)
    def updateSerialConnection(self, worker, state):
        """Update the connection status"""
        if self.hasView():
            self.getView().updateView()

    @QtCore.pyqtSlot(object)
    def updateConfig(self, config):
        """Process update from Config"""
        if self.hasView():
            self.getView().updateView()
        # reload and connect serial model
        if self.hasSerialModel():
            self.resetSerialModel()

    @QtCore.pyqtSlot()
    def updateWavelength(self):
        """Pass new wavelength to """
        w = self.getView().getActiveWaveLength()
        self.getIndependentModel().setActiveWavelength(w)
        self.getCultivatorModel().setActiveWavelength(w)
        self.getPredictionModel().setActiveWavelength(w)

    @QtCore.pyqtSlot()
    def editSerialConnection(self):
        SerialController.start(
            model=self.getSerialModel(), worker=self.getSerialWorker(), parent=self, app=self.getApplication()
        )

    @QtCore.pyqtSlot()
    def learnSettings(self):
        """Learns the correct settings for measuring the serial """
        LearnController.start(
            worker=self.getSerialWorker(), parent=self, app=self.getApplication()
        )

    def editCalibration(self, index=None, calibration=None):
        if index is not None and calibration is None:
            calibration = self.getPredictionModel().getCalibration(index=index)
        if calibration is not None:
            # edit
            CalibrationController.start(
                calibration=calibration, parent=self, app=self.getApplication()
            )

    # SLOTS FOR SERIAL COMMUNICATION

    @QtCore.pyqtSlot()
    def connectSerial(self):
        result = False
        if self.hasSerialWorker():
            # create wait view
            job = self.getSerialWorker().createJob(
                fun=self.getSerialWorker()._connect
            )
            job.setProgressTitle("Connecting, please wait..")
            job.openProgressDialog(value_signal=self.getSerialWorker().progressed, parent=self.getView())
            # start job
            self.getSerialWorker().progressed.connect(job.getProgressDialog().setValue)
            self.getSerialWorker().addJob(job)
            while not job.isFinished():
                # wait
                self.getApplication().processEvents()
            # remove finished job from queue
            self.getSerialWorker().getFinishedJob(block=False)
            job.closeProgressDialog(value_signal=self.getSerialWorker().progressed)
            if not job.succeeded() or not job.getResult():
                self.getView().showAlert(
                    title="Connecting failed",
                    text="Was unable to connect to Multi-Cultivator.",
                    information=
                    "Tried to connect to port: %s.\n"
                    "Job was %s successful and returned %s"
                    "" % (
                        self.getSerialModel().getPort(),
                        "" if job.succeeded() else "NOT",
                        job.getResult()
                    ),
                    parent=self.getView()
                )
            # end if

    # end if

    @QtCore.pyqtSlot()
    def disconnectSerial(self):
        if self.hasSerialWorker():
            # create job view
            job = self.getSerialWorker().createJob(
                fun=self.getSerialWorker()._disconnect
            )
            # set wait view parameters
            job.setProgressTitle("Disconnecting, please wait..")
            job.openProgressDialog(parent=self.getView())
            # start job
            self.getSerialWorker().progressed.connect(job.getProgressDialog().setValue)
            self.getSerialWorker().addJob(job)
            while not job.isFinished():
                # wait
                self.getApplication().processEvents()
            # remove finished job from queue
            self.getSerialWorker().getFinishedJob(block=False)
            job.closeProgressDialog(value_signal=self.getSerialWorker().progressed)
            if not job.succeeded() or not job.getResult():
                self.getView().showAlert(
                    title="Disconnecting failed",
                    text="Was unable to disconnect from the MultiCultivator.",
                    parent=self.getView()
                )
        return

    @QtCore.pyqtSlot()
    def measureSerial(self):
        """Will measure the OD in all the channels"""
        if self.hasSerialWorker():
            # create wait view
            job = self.getSerialWorker().createJob(
                fun=self.getSerialWorker()._measure,
                channels=None, wavelength=self.getView().getActiveWaveLength()
            )
            job.setProgressTitle(
                "Measuring Optical Density (nm= %s), please wait.." % self.getView().getActiveWaveLength())
            job.openProgressDialog(parent=self.getView())
            # start job
            self.getSerialWorker().progressed.connect(job.getProgressDialog().setValue)
            self.getSerialWorker().addJob(job)
            while not job.isFinished():
                self.getApplication().processEvents()
            # remove finished job from queue
            self.getSerialWorker().getFinishedJob(block=False)
            job.closeProgressDialog(value_signal=self.getSerialWorker().progressed)
            if job.succeeded():
                self.processMeasurements(job.getResult())
            else:
                self.getView().showAlert(
                    title="OD Measurement", text="Was unable to measure the OD", parent=self.getView()
                )
        return

    def processMeasurements(self, measurements=None):
        """

        :param job:
        :type job: calibrator.controllers.base.BaseJob
        :return:
        :rtype:
        """
        if measurements is None:
            measurements = []
        if len(measurements) > 0:
            # add row
            self.getCultivatorModel().addRow()
            row = self.getCultivatorModel().lastRow()
            for m in measurements:
                col = self.getCultivatorModel().getChannels().index(m.getChannel())
                self.getCultivatorModel().setMeasurement(row=row, column=col, measurement=m)

    @QtCore.pyqtSlot(QtCore.QModelIndex)
    def calibrateModels(self, index=None):
        """Will adjust the intercept of the calibration models"""
        if index is None:
            # do for every channel
            for column in range(len(self.getChannels())):
                self.calibrateModel(column=column)
        else:
            self.calibrateModel(index=index)

    def calibrateModel(self, index=None, column=None):
        """Adjust the intercept of one channel"""
        self.getPredictionModel().calibrateModel(index=index, column=column)

    def getView(self):
        """Returns the view connected to this controller

        :return:
        :rtype: calibrator.views.main.MainView
        """
        return self._view

    def getSerialModel(self):
        """Returns the serial connection model

        :rtype: calibrator.models.serial.SerialConnectionModel
        """
        return self._serial_model

    def hasSerialModel(self):
        return self.getSerialModel() is not None

    def getSerialWorker(self):
        """Returns the serial connection worker

        :rtype: calibrator.controllers.serial.SerialWorker
        """
        return self._serial_worker

    def hasSerialWorker(self):
        return self.getSerialWorker() is not None

    def runningSerialWorker(self):
        result = False
        if self.hasSerialWorker():
            result = self.getSerialWorker().isRunning()
        return result

    def stopSerialWorker(self):
        result = True
        if self.hasSerialWorker() and self.runningSerialWorker():
            w = self.getSerialWorker()
            w.setExiting(True)
            result = w.wait(5000)
            if not result:
                self.getLog().warning("%s did not stop in time, terminate it" % w.objectName())
                w.terminate()
                result = w.wait()
        return result

    def getCultivatorModel(self):
        """Returns the cultivator measurement model

        :return:
        :rtype: calibrator.models.measurement.ODMeasurementTableModel
        """
        return self._mc_model

    def hasCultivatorModel(self):
        return self.getCultivatorModel() is not None

    def hasCultivatorData(self):
        return self._has_mc_data is True

    def getIndependentModel(self):
        """Returns the independent measurement model

        :return:
        :rtype: calibrator.models.measurement.ODMeasurementTableModel
        """
        return self._ind_model

    def hasIndependentModel(self):
        return self.getIndependentModel() is not None

    def hasIndependentData(self):
        return self._has_ind_data is True

    def getPredictionModel(self):
        """Returns the prediction measurement model

        :return:
        :rtype: calibrator.models.calibration.CalibrationTableModel
        """
        return self._pred_model

    def hasPredictionModel(self):
        return self.getPredictionModel() is not None

    def hasPredictionData(self):
        return self._has_pred_data is True

    def askToSave(self):
        result = False
        response = self.getView().askToSave(os.path.split(self.getXMLPath())[1])
        if response is not None:
            if response is True:
                self.saveXML()
            result = True
        return result

    def getXMLPath(self):
        return self._xml_path

    def saveXML(self, saveAs=False):
        """
        Write config object to xml file
        :return:
        :rtype:
        """
        result = False
        # first save the model
        self.getSerialModel().save(self.getConfig())
        # save the calibration models
        self.saveCalibrationModels()
        xml_path = self.getConfig().getPath()
        if saveAs:
            # noinspection PyArgumentList
            save_path, result = QtGui.QFileDialog.getSaveFileNameAndFilter(
                parent=self.getView(),
                caption="Save Config File as...",
                directory=os.path.dirname(xml_path if xml_path is not None else __file__),
                filter="XML Config file (*.xml)"
            )
            xml_path = str(save_path)
        if xml_path is not None and xml_path != "":
            result = self.getConfig().save(xml_path)
            if result:
                self.getView().showStatusBarMessage("Saved {}".format(self.getConfig().getPath()))
                self._saved_xml = self.getConfig().getXML()
                dialog = self.getView().createMessageBox(
                    title="Configuration File Saved",
                    text="Tell GIT about your change NOW.\nSee details for instructions",
                    information="git commit -am \'changed %s\'\ngit pull\ngit push!" % os.path.split(xml_path)[1]
                )
                dialog.setIcon(QtGui.QMessageBox.Information)
                dialog.setStandardButtons(QtGui.QMessageBox.Ok | QtGui.QMessageBox.Default)
                dialog.exec_()
        return result

    @QtCore.pyqtSlot()
    def openXML(self, xml_path=None):
        """Open an XML file from a location

        :return: Whether it loaded
        :rtype: bool
        """
        result = False
        if xml_path is None:
            xml_path, response = QtGui.QFileDialog.getOpenFileNameAndFilter(
                parent=self.getView(),
                caption="Select a config file to open",
                directory=self.getRootPath(),
                filter="XML Config file (*.xml)"
            )
            xml_path = str(xml_path)
        if xml_path is not None and xml_path != "":
            self.loadXML(xml_path)
            result = True
        return result

    def loadXML(self, xml_file):
        result = False
        if xml_file != self.getXMLPath() and xml_file is not None and os.path.isfile(xml_file):
            # check status current document
            if self.hasConfig() and not self.isSaved():
                self.askToSave()
            # if we have a file -> load the xml -> if successful set hasConfig is True
            self._xml_path = xml_file
            self.setConfig(xCC.createFromFile(self.getXMLPath()))
            if not self.hasConfig():
                self.getLog().error("Unable to load Configuration file at %s" % self.getXMLPath())
                self.getView().showAlert(
                    "Load Configuration File",
                    "Failed to load configuration file at",
                    self.getXMLPath(),
                    self.getView()
                )
            else:
                result = True
                self.getView().showStatusBarMessage("Loaded configuration at {}".format(self.getConfig().getPath()))
        return result

    def saveCalibrationModels(self):
        """Save all calibration models"""
        for w in self.getWavelengths():
            for c in self.getChannels():
                self.saveCalibrationModel(channel=c, wavelength=w)
        return

    def saveCalibrationModel(self, channel, wavelength=None):
        """Saves """
        column, channel_e, calibration = None, None, None
        if channel in self.getChannels():
            column = self.getChannels().index(channel)
            channel_e = self.getConfig().getChannelElement(channel=channel)
        if column is not None:
            calibration = self.getPredictionModel().getCalibration(row=0, column=column, wavelength=wavelength)
        if channel_e is not None and calibration is not None:
            calibrations_e = self.findCalibrationElements(channel=channel_e, wavelength=wavelength)
            if len(calibrations_e) == 0:
                calibrations_e = self.findCalibrationElements(channel=channel_e, wavelength="self")
            if len(calibrations_e) == 0:
                # create
                calibrations_e = channel_e.findall("./u:calibrations", self.getConfig().getNameSpace())
                if len(calibrations_e) > 0:
                    self.createCalibrationElement(
                        calibration, calibrations_e[0], channel, wavelength
                    )
                else:
                    calibration_e = etree.SubElement(channel_e, "calibrations")
                    self.createCalibrationElement(
                        calibration, calibration_e, channel, wavelength
                    )
            else:
                # replace use first element
                self.replaceCalibration(calibration, channel, wavelength)
            # end if
        # end if
        return

    def findCalibrationsElement(self, channel):
        result = None
        channel_e = channel
        if isinstance(channel, int):
            channel_e = self.getConfig().getChannelElement(channel=channel)
        xpath = "./u:calibrations"
        try:
            results = channel_e.findall(xpath, self.getConfig().getNameSpace())
            if len(results) > 0:
                result = results[0]
        except SyntaxError as se:
            self.getLog().warning("Syntax Error:\n %s\n in %s" % (se, xpath))
        return result

    def findCalibrationElements(self, channel, wavelength):
        result = []
        channel_e = channel
        if isinstance(channel, int):
            channel_e = self.getConfig().getChannelElement(channel=channel)
        xpath = "./u:calibrations/u:calibration[@source='{}'][@variable='{}']".format(wavelength, 'od')
        try:
            result = channel_e.findall(xpath, self.getConfig().getNameSpace())
        except SyntaxError as se:
            self.getLog().warning("Syntax Error:\n %s\n in %s" % (se, xpath))
        return result

    def createCalibrationElement(self, calibration, calibrations_e, channel, wavelength):
        """Creates a new calibration element from model"""
        result = False
        calibration_e = etree.SubElement(calibrations_e, "calibration")
        # set parameters
        calibration_e.set("source", str(wavelength))
        calibration_e.set("variable", "od")
        calibration_e.set("active", "true" if calibration.isActive() else "false")
        # add polynomials
        for polynomial in calibration.getPolynomials():
            poly_e = etree.SubElement(calibration_e, "polynomial")
            poly_e.set("domain", polynomial.getDomain().__str__())
            for order in range(len(polynomial.getCoefficients())):
                coef = polynomial.getCoefficient(order)
                coef_e = etree.SubElement(poly_e, "coefficient")
                coef_e.set("order", str(order))
                coef_e.text = "{:.8f}".format(coef)
            result = True
        # end for
        # end for
        return result

    def replaceCalibration(self, calibration, channel, wavelength):
        """Removes old (duplicate) calibrations and stores the new"""
        result = False
        # find and remove duplicates
        self.removeCalibrations(channel, wavelength)
        if wavelength == 720:
            self.removeCalibrations(channel, "self")
        # get parent
        calibrations_e = self.findCalibrationsElement(channel)
        if calibrations_e is not None:
            # add new
            result = self.createCalibrationElement(calibration, calibrations_e, channel, wavelength)
        return result

    def removeCalibrations(self, channel, wavelength):
        """Removes all calibration elements that of this channel for that wavelength"""
        result = True
        calibration_es = self.findCalibrationElements(channel, wavelength)
        for calibration_e in calibration_es:
            # remove duplicates
            result = self.removeCalibrationElement(calibration_e) and result
        return result

    def removeCalibrationElement(self, element):
        """ Removes the element from the XML DOM

        :param element:
        :type element: lxml.
        :return:
        :rtype:
        """
        element.getparent().remove(element)
        return True

    def replaceCalibrationElement(self, calibration, element, channel, wavelength):
        """Removes an old calibration element and """
        self.removeCalibrations(channel, wavelength)
        return self.createCalibrationElement(calibration, element.getparent(), channel, wavelength)

    def isSaved(self):
        result = True
        if self.hasConfig():
            result = self.getConfig().getXML() == self._saved_xml
        return result

    def setConfig(self, config):
        self._config = config
        if self.hasConfig():
            self.resetCalibrationModels()
            self._saved_xml = self.getConfig().getXML()
        # notify view
        self.config_changed.emit(self)

    def resetCalibrationModels(self, wavelengths=None):
        if self.hasPredictionModel() and self.hasConfig():
            if wavelengths is None:
                wavelengths = self.getPredictionModel().getWavelengths()
            if isinstance(wavelengths, (float, int)):
                wavelengths = [wavelengths]
            self.getPredictionModel().clear()
            for c in self.getChannels():
                # pre load all the calibrations
                for w in wavelengths:
                    self.resetCalibrationModel(channel=c, wavelength=w)
                # end for
        # end if
        return

    def resetCalibrationModel(self, channel, wavelength=None):
        if self.hasPredictionModel() and self.hasConfig():
            if wavelength is None:
                wavelength = self.getPredictionModel().getActiveWavelength()
            calibration = self.getConfig().loadODCalibration(channel=channel, wavelength=wavelength)
            col = self.getPredictionModel().getChannels().index(channel)
            if int(wavelength) in self.getPredictionModel().getWavelengths() and calibration is not None:
                if self.getPredictionModel().countRows(wavelength=wavelength) == 0:
                    self.getPredictionModel().addRow(wavelength=wavelength)
                self.getPredictionModel().setItem(row=0, column=col, wavelength=wavelength, value=calibration)
            # end if
        return

    def getConfig(self):
        """
        :rtype: uvaCultivator.uvaCultivatorConfig.xmlCultivatorConfig.xmlCultivatorConfig
        """
        return self._config

    def hasConfig(self):
        return self._config is not None

    def getRootPath(self):
        """Returns the root path or the directory path of the current file"""
        # use object default
        result = self._root_path
        # if unset, use directory name of this file
        if result is None:
            result = os.path.dirname(__file__)
        # if another file is open, use the directory name of that file
        if self.getXMLPath() is not None:
            result = os.path.dirname(self.getXMLPath())
        return result

    def setRootPath(self, p):
        if os.path.exists(p):
            if not os.path.isdir(p):
                # retry with dirname
                self.setRootPath(os.path.dirname(p))
            else:
                # exists and is a directory
                self._root_path = p
        return self

    def isConnected(self):
        result = False
        if self.hasSerialWorker():
            result = self.getSerialWorker().isConnected()
        return result

    def getChannels(self):
        return self._channels

    def getWavelengths(self):
        return self._wavelengths
