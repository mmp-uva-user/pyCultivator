# coding=utf-8

__author__ = 'Joeri Jongbloets <j.a.jongbloets@uva.nl>'

# import sys  # , re
from PyQt4 import QtCore, QtGui
from uvaCultivator import uvaLog


def show_waiting(fun):
    # noinspection PyArgumentList
    def new_function(self):
        QtGui.QApplication.setOverrideCursor(QtGui.QCursor(QtCore.Qt.WaitCursor))
        try:
            fun(self)
        except:
            raise
        finally:
            QtGui.QApplication.restoreOverrideCursor()

    return new_function


class BaseView(object):
    """
    Basic View object
    """

    _log = None
    DT_FORMAT = ""

    def __init__(self, controller, parent=None):
        self._ui = None
        self._controller = controller
        self._parent = parent
        if self.getParent() is None:
            self._parent = self._controller

    @classmethod
    def getLog(cls):
        if cls._log is None:
            cls._log = uvaLog.getLogger(cls.__name__)
        return cls._log

    def getUi(self):
        """
        :rtype: PyQt4.QtGui.QWidget.QWidget
        """
        return self._ui

    def hasUi(self):
        self.getUi() is not None

    def getParent(self):
        """Returns the parent of this view (a controller)

        :return:
        :rtype: calibrator.controllers.base.BaseController
        """
        return self._parent

    def hasParent(self):
        return self.getParent() is not None

    def getApplication(self):
        app = None
        if self.hasParent():
            app = self.getParent().getApplication()
        if app is None:
            app = QtGui.QApplication
        return app

    def getController(self):
        """
        :rtype:
        """
        return self._controller

    def hasController(self):
        return self.getController() is not None

    @classmethod
    def getAndValidateValue(cls, get_func, set_func, val_type=str, default_value=""):
        """
        Will cast a value, obtained through get_func, to a given type. When it fails or when the value is "",
        the default_value is set and returned. The source is updated to the default value via set_func
        :param val_type: str, int, float, datetime.datetime
        :param default_value: str, int, float, datetime.datetime
        """
        result = ""
        try:
            result = val_type(get_func())
        except Exception as e:
            cls.getLog().warning("Error occurred:\n{}".format(e))
        if result == "":
            set_func(default_value)
            result = default_value
        return result

    @classmethod
    def showWait(cls, job, title, text, information=None, parent=None):
        """Shows a dialog while the job is running

        :type job: calibrator.controllers.base.BaseJob
        """
        alert = QtGui.QMessageBox(parent)
        alert.setIcon(QtGui.QMessageBox.Information)
        alert.setWindowTitle(title)
        alert.setText(text)
        if information is not None:
            alert.setInformativeText(information)
        # show
        alert.show()
        while not job.isFinished():
            job.finished.wait()
        alert.hide()

    @classmethod
    def showAlert(cls, title, text, information=None, parent=None):
        alert = QtGui.QMessageBox(parent)
        alert.setIcon(QtGui.QMessageBox.Critical)
        alert.setText(title)
        alert.setInformativeText(text)
        if information is not None:
            alert.setDetailedText(information)
        alert.setStandardButtons(QtGui.QMessageBox.Ok | QtGui.QMessageBox.Default)
        return alert.exec_()

    @staticmethod
    def createMessageBox(title, text, information=None, parent=None, icon=QtGui.QMessageBox.Question):
        dialog = QtGui.QMessageBox(parent)
        dialog.setIcon(icon)
        dialog.setText(title)
        dialog.setInformativeText(text)
        if information is not None:
            dialog.setDetailedText(information)
        dialog.setStandardButtons(QtGui.QMessageBox.Yes | QtGui.QMessageBox.No)
        dialog.setDefaultButton(QtGui.QMessageBox.Yes)
        return dialog

    @classmethod
    def askConfirmation(cls, message, question="Are you sure?"):
        """
        Show a confirmation box with the suplied message
        :param message: Message to show
        :type message: str
        :return: Whether the user clicked Yes
        :rtype: bool
        """
        dialog = cls.createMessageBox(title=question, text=message)
        dialog.setIcon(QtGui.QMessageBox.Question)
        return dialog.exec_() == QtGui.QMessageBox.Yes

    @classmethod
    def askToSave(cls, subject="this file"):
        """
        Asks the user to save the subject. Returns a bool if the users chooses between Save or Discard, otherwise
        return None (i.e. no choice was made, so Cancel was pressed)
        :rtype: None, bool
        """
        result = None
        dialog = cls.createMessageBox(
            "Do you want to save?",
            "You modified {}, do you want to save it?".format(subject)
        )
        dialog.setStandardButtons(QtGui.QMessageBox.Save | QtGui.QMessageBox.Discard | QtGui.QMessageBox.Cancel)
        dialog.setDefaultButton(QtGui.QMessageBox.Save)
        dialog.setIcon(QtGui.QMessageBox.Question)
        response = dialog.exec_()
        if response == QtGui.QMessageBox.Save:
            result = True
        elif response == QtGui.QMessageBox.Discard:
            result = False
        return result
