# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'calibration.ui'
#
# Created by: PyQt4 UI code generator 4.11.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_Dialog(object):
    def setupUi(self, Dialog):
        Dialog.setObjectName(_fromUtf8("Dialog"))
        Dialog.resize(489, 372)
        self.gridLayout = QtGui.QGridLayout(Dialog)
        self.gridLayout.setObjectName(_fromUtf8("gridLayout"))
        self.gbPolynomials = QtGui.QGroupBox(Dialog)
        self.gbPolynomials.setObjectName(_fromUtf8("gbPolynomials"))
        self.gridLayout_3 = QtGui.QGridLayout(self.gbPolynomials)
        self.gridLayout_3.setObjectName(_fromUtf8("gridLayout_3"))
        self.lvPolynomials = QtGui.QListView(self.gbPolynomials)
        self.lvPolynomials.setObjectName(_fromUtf8("lvPolynomials"))
        self.gridLayout_3.addWidget(self.lvPolynomials, 0, 0, 1, 1)
        self.gridLayout.addWidget(self.gbPolynomials, 1, 0, 1, 1)
        self.gbPolySettings = QtGui.QGroupBox(Dialog)
        self.gbPolySettings.setObjectName(_fromUtf8("gbPolySettings"))
        self.gridLayout_5 = QtGui.QGridLayout(self.gbPolySettings)
        self.gridLayout_5.setObjectName(_fromUtf8("gridLayout_5"))
        self.lvCoefficients = QtGui.QListView(self.gbPolySettings)
        self.lvCoefficients.setObjectName(_fromUtf8("lvCoefficients"))
        self.gridLayout_5.addWidget(self.lvCoefficients, 2, 2, 4, 1)
        self.lbFormula = QtGui.QLabel(self.gbPolySettings)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Preferred, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.lbFormula.sizePolicy().hasHeightForWidth())
        self.lbFormula.setSizePolicy(sizePolicy)
        self.lbFormula.setStyleSheet(_fromUtf8(""))
        self.lbFormula.setText(_fromUtf8("1x"))
        self.lbFormula.setTextFormat(QtCore.Qt.AutoText)
        self.lbFormula.setAlignment(QtCore.Qt.AlignCenter)
        self.lbFormula.setObjectName(_fromUtf8("lbFormula"))
        self.gridLayout_5.addWidget(self.lbFormula, 0, 0, 1, 3)
        self.cbIncludesStart = QtGui.QCheckBox(self.gbPolySettings)
        self.cbIncludesStart.setObjectName(_fromUtf8("cbIncludesStart"))
        self.gridLayout_5.addWidget(self.cbIncludesStart, 2, 0, 1, 2)
        self.dsbEndValue = QtGui.QDoubleSpinBox(self.gbPolySettings)
        self.dsbEndValue.setDecimals(4)
        self.dsbEndValue.setMinimum(-100.0)
        self.dsbEndValue.setMaximum(100.0)
        self.dsbEndValue.setObjectName(_fromUtf8("dsbEndValue"))
        self.gridLayout_5.addWidget(self.dsbEndValue, 5, 1, 1, 1)
        self.cbStartInfinite = QtGui.QCheckBox(self.gbPolySettings)
        self.cbStartInfinite.setObjectName(_fromUtf8("cbStartInfinite"))
        self.gridLayout_5.addWidget(self.cbStartInfinite, 3, 0, 1, 1)
        self.cbEndInfinite = QtGui.QCheckBox(self.gbPolySettings)
        self.cbEndInfinite.setObjectName(_fromUtf8("cbEndInfinite"))
        self.gridLayout_5.addWidget(self.cbEndInfinite, 5, 0, 1, 1)
        self.cbIncludesEnd = QtGui.QCheckBox(self.gbPolySettings)
        self.cbIncludesEnd.setObjectName(_fromUtf8("cbIncludesEnd"))
        self.gridLayout_5.addWidget(self.cbIncludesEnd, 4, 0, 1, 2)
        self.dsbStartValue = QtGui.QDoubleSpinBox(self.gbPolySettings)
        self.dsbStartValue.setDecimals(4)
        self.dsbStartValue.setMinimum(-100.0)
        self.dsbStartValue.setMaximum(100.0)
        self.dsbStartValue.setObjectName(_fromUtf8("dsbStartValue"))
        self.gridLayout_5.addWidget(self.dsbStartValue, 3, 1, 1, 1)
        self.lbDomain = QtGui.QLabel(self.gbPolySettings)
        self.lbDomain.setAlignment(QtCore.Qt.AlignCenter)
        self.lbDomain.setObjectName(_fromUtf8("lbDomain"))
        self.gridLayout_5.addWidget(self.lbDomain, 1, 0, 1, 3)
        self.gridLayout.addWidget(self.gbPolySettings, 1, 1, 1, 1)
        self.gbSettings = QtGui.QGroupBox(Dialog)
        self.gbSettings.setObjectName(_fromUtf8("gbSettings"))
        self.gridLayout_2 = QtGui.QGridLayout(self.gbSettings)
        self.gridLayout_2.setObjectName(_fromUtf8("gridLayout_2"))
        self.lbSource = QtGui.QLabel(self.gbSettings)
        self.lbSource.setObjectName(_fromUtf8("lbSource"))
        self.gridLayout_2.addWidget(self.lbSource, 1, 0, 1, 1)
        self.cbActive = QtGui.QCheckBox(self.gbSettings)
        self.cbActive.setObjectName(_fromUtf8("cbActive"))
        self.gridLayout_2.addWidget(self.cbActive, 2, 0, 1, 3)
        self.leSource = QtGui.QLineEdit(self.gbSettings)
        self.leSource.setEnabled(False)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.leSource.sizePolicy().hasHeightForWidth())
        self.leSource.setSizePolicy(sizePolicy)
        self.leSource.setMaximumSize(QtCore.QSize(50, 16777215))
        self.leSource.setObjectName(_fromUtf8("leSource"))
        self.gridLayout_2.addWidget(self.leSource, 1, 2, 1, 1)
        self.lbIdentifier = QtGui.QLabel(self.gbSettings)
        self.lbIdentifier.setObjectName(_fromUtf8("lbIdentifier"))
        self.gridLayout_2.addWidget(self.lbIdentifier, 0, 0, 1, 1)
        self.leIdentifier = QtGui.QLineEdit(self.gbSettings)
        self.leIdentifier.setObjectName(_fromUtf8("leIdentifier"))
        self.gridLayout_2.addWidget(self.leIdentifier, 0, 2, 1, 1)
        self.gridLayout.addWidget(self.gbSettings, 0, 0, 1, 1)
        self.buttonBox = QtGui.QDialogButtonBox(Dialog)
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtGui.QDialogButtonBox.Cancel|QtGui.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName(_fromUtf8("buttonBox"))
        self.gridLayout.addWidget(self.buttonBox, 2, 0, 1, 2)

        self.retranslateUi(Dialog)
        QtCore.QObject.connect(self.buttonBox, QtCore.SIGNAL(_fromUtf8("accepted()")), Dialog.accept)
        QtCore.QObject.connect(self.buttonBox, QtCore.SIGNAL(_fromUtf8("rejected()")), Dialog.reject)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        Dialog.setWindowTitle(_translate("Dialog", "Dialog", None))
        self.gbPolynomials.setTitle(_translate("Dialog", "Domains", None))
        self.gbPolySettings.setTitle(_translate("Dialog", "Polynomial Settings", None))
        self.cbIncludesStart.setText(_translate("Dialog", "Includes start value", None))
        self.cbStartInfinite.setText(_translate("Dialog", "-Infinite", None))
        self.cbEndInfinite.setText(_translate("Dialog", "Infinite", None))
        self.cbIncludesEnd.setText(_translate("Dialog", "Includes end value", None))
        self.lbDomain.setText(_translate("Dialog", "(,)", None))
        self.gbSettings.setTitle(_translate("Dialog", "Settings", None))
        self.lbSource.setText(_translate("Dialog", "Source", None))
        self.cbActive.setText(_translate("Dialog", "is Active?", None))
        self.lbIdentifier.setText(_translate("Dialog", "Identifier", None))

