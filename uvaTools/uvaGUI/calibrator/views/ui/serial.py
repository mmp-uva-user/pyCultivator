# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'serial.ui'
#
# Created by: PyQt4 UI code generator 4.11.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_Dialog(object):
    def setupUi(self, Dialog):
        Dialog.setObjectName(_fromUtf8("Dialog"))
        Dialog.resize(510, 325)
        self.gridLayout = QtGui.QGridLayout(Dialog)
        self.gridLayout.setContentsMargins(-1, -1, -1, 0)
        self.gridLayout.setObjectName(_fromUtf8("gridLayout"))
        self.gbSerial = QtGui.QGroupBox(Dialog)
        self.gbSerial.setMinimumSize(QtCore.QSize(233, 0))
        self.gbSerial.setObjectName(_fromUtf8("gbSerial"))
        self.gridLayout_2 = QtGui.QGridLayout(self.gbSerial)
        self.gridLayout_2.setObjectName(_fromUtf8("gridLayout_2"))
        self.pbTestSerial = QtGui.QPushButton(self.gbSerial)
        self.pbTestSerial.setObjectName(_fromUtf8("pbTestSerial"))
        self.gridLayout_2.addWidget(self.pbTestSerial, 6, 0, 1, 2)
        self.cbFake = QtGui.QCheckBox(self.gbSerial)
        self.cbFake.setObjectName(_fromUtf8("cbFake"))
        self.gridLayout_2.addWidget(self.cbFake, 4, 0, 1, 2)
        self.lbRate = QtGui.QLabel(self.gbSerial)
        self.lbRate.setObjectName(_fromUtf8("lbRate"))
        self.gridLayout_2.addWidget(self.lbRate, 1, 0, 1, 1)
        self.lePort = QtGui.QLineEdit(self.gbSerial)
        self.lePort.setObjectName(_fromUtf8("lePort"))
        self.gridLayout_2.addWidget(self.lePort, 0, 1, 1, 1)
        self.lbPort = QtGui.QLabel(self.gbSerial)
        self.lbPort.setObjectName(_fromUtf8("lbPort"))
        self.gridLayout_2.addWidget(self.lbPort, 0, 0, 1, 1)
        self.cbRate = QtGui.QComboBox(self.gbSerial)
        self.cbRate.setEnabled(False)
        self.cbRate.setEditable(False)
        self.cbRate.setObjectName(_fromUtf8("cbRate"))
        self.cbRate.addItem(_fromUtf8(""))
        self.cbRate.addItem(_fromUtf8(""))
        self.gridLayout_2.addWidget(self.cbRate, 1, 1, 1, 1)
        spacerItem = QtGui.QSpacerItem(20, 40, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        self.gridLayout_2.addItem(spacerItem, 5, 0, 1, 1)
        self.gridLayout.addWidget(self.gbSerial, 0, 0, 1, 1)
        self.groupBox_2 = QtGui.QGroupBox(Dialog)
        self.groupBox_2.setMinimumSize(QtCore.QSize(233, 0))
        self.groupBox_2.setObjectName(_fromUtf8("groupBox_2"))
        self.gridLayout_3 = QtGui.QGridLayout(self.groupBox_2)
        self.gridLayout_3.setObjectName(_fromUtf8("gridLayout_3"))
        self.lbAggregation = QtGui.QLabel(self.groupBox_2)
        self.lbAggregation.setObjectName(_fromUtf8("lbAggregation"))
        self.gridLayout_3.addWidget(self.lbAggregation, 2, 0, 1, 1)
        self.cbAggregation = QtGui.QComboBox(self.groupBox_2)
        self.cbAggregation.setObjectName(_fromUtf8("cbAggregation"))
        self.gridLayout_3.addWidget(self.cbAggregation, 2, 1, 1, 1)
        self.pbTestMeasurement = QtGui.QPushButton(self.groupBox_2)
        self.pbTestMeasurement.setObjectName(_fromUtf8("pbTestMeasurement"))
        self.gridLayout_3.addWidget(self.pbTestMeasurement, 5, 0, 1, 2)
        self.lbRepeats = QtGui.QLabel(self.groupBox_2)
        self.lbRepeats.setObjectName(_fromUtf8("lbRepeats"))
        self.gridLayout_3.addWidget(self.lbRepeats, 0, 0, 1, 1)
        self.lbSamples = QtGui.QLabel(self.groupBox_2)
        self.lbSamples.setObjectName(_fromUtf8("lbSamples"))
        self.gridLayout_3.addWidget(self.lbSamples, 1, 0, 1, 1)
        self.sbRepeats = QtGui.QSpinBox(self.groupBox_2)
        self.sbRepeats.setObjectName(_fromUtf8("sbRepeats"))
        self.gridLayout_3.addWidget(self.sbRepeats, 0, 1, 1, 1)
        self.sbGasPause = QtGui.QSpinBox(self.groupBox_2)
        self.sbGasPause.setObjectName(_fromUtf8("sbGasPause"))
        self.gridLayout_3.addWidget(self.sbGasPause, 3, 1, 1, 1)
        self.lbGasPause = QtGui.QLabel(self.groupBox_2)
        self.lbGasPause.setObjectName(_fromUtf8("lbGasPause"))
        self.gridLayout_3.addWidget(self.lbGasPause, 3, 0, 1, 1)
        self.sbSamples = QtGui.QSpinBox(self.groupBox_2)
        self.sbSamples.setObjectName(_fromUtf8("sbSamples"))
        self.gridLayout_3.addWidget(self.sbSamples, 1, 1, 1, 1)
        spacerItem1 = QtGui.QSpacerItem(20, 40, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        self.gridLayout_3.addItem(spacerItem1, 4, 0, 1, 1)
        self.gridLayout.addWidget(self.groupBox_2, 0, 1, 1, 1)
        self.buttonBox = QtGui.QDialogButtonBox(Dialog)
        self.buttonBox.setStatusTip(_fromUtf8(""))
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtGui.QDialogButtonBox.Cancel|QtGui.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName(_fromUtf8("buttonBox"))
        self.gridLayout.addWidget(self.buttonBox, 1, 0, 1, 2)
        self.pbProgress = QtGui.QProgressBar(Dialog)
        self.pbProgress.setProperty("value", 24)
        self.pbProgress.setTextVisible(False)
        self.pbProgress.setObjectName(_fromUtf8("pbProgress"))
        self.gridLayout.addWidget(self.pbProgress, 3, 0, 1, 2)
        self.lbStatus = QtGui.QLabel(Dialog)
        self.lbStatus.setMaximumSize(QtCore.QSize(16777215, 21))
        self.lbStatus.setObjectName(_fromUtf8("lbStatus"))
        self.gridLayout.addWidget(self.lbStatus, 2, 0, 1, 2)

        self.retranslateUi(Dialog)
        QtCore.QObject.connect(self.buttonBox, QtCore.SIGNAL(_fromUtf8("accepted()")), Dialog.accept)
        QtCore.QObject.connect(self.buttonBox, QtCore.SIGNAL(_fromUtf8("rejected()")), Dialog.reject)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        Dialog.setWindowTitle(_translate("Dialog", "Edit Serial Settings", None))
        self.gbSerial.setTitle(_translate("Dialog", "Serial link settings", None))
        self.pbTestSerial.setText(_translate("Dialog", "Test Connection", None))
        self.cbFake.setText(_translate("Dialog", "Simulate", None))
        self.lbRate.setText(_translate("Dialog", "Baudrate", None))
        self.lbPort.setText(_translate("Dialog", "Serial port", None))
        self.cbRate.setItemText(0, _translate("Dialog", "9600", None))
        self.cbRate.setItemText(1, _translate("Dialog", "115200", None))
        self.groupBox_2.setTitle(_translate("Dialog", "Measurement settings", None))
        self.lbAggregation.setText(_translate("Dialog", "Aggregation", None))
        self.pbTestMeasurement.setText(_translate("Dialog", "Test Measurement", None))
        self.lbRepeats.setText(_translate("Dialog", "OD repeats", None))
        self.lbSamples.setText(_translate("Dialog", "Samples", None))
        self.lbGasPause.setText(_translate("Dialog", "Gas  pause (sec.)", None))
        self.lbStatus.setText(_translate("Dialog", "Status message bar", None))

